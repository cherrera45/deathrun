#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <cstrike>
#include <deathrun_world>
#include <fun>

new g_iKnife[ 33 ], g_iMaxplayers, g_item;

new const szKnife_v[] = "v_hacha";
new const szKnife_p[] = "p_hacha";

public plugin_init()
{
	register_plugin("knife mdl", "1.0", "Hypnotize divstarproject.com");
	RegisterHam(Ham_Item_Deploy, "weapon_knife", "ham_KnifeDeployPost", true);
	register_logevent( "event_round_start", 2, "1=Round_Start" );

	g_iMaxplayers = get_maxplayers();

	g_item = register_item("Hacha", 1, 0, KNIFE, ADMIN_ALL);
}
public dr_item(id, item, tipo)
{
	if( tipo != KNIFE )
		return;
		
	if( g_item != item )
		return;

	g_iKnife[ id ] = 1;		
	give_item(id, "weapon_knife");
	engclient_cmd(id, "weapon_knife");
	return;
}	

public plugin_precache()
{
  	static buffer[128];
	
	formatex( buffer, charsmax( buffer ), "models/%s.mdl", szKnife_v);
	precache_model(buffer);
	formatex( buffer, charsmax( buffer ), "models/%s.mdl", szKnife_p);
	precache_model(buffer);
}
public event_round_start()
{
	for(new i = 1; i <= g_iMaxplayers; ++i)
	{
		g_iKnife[ i ] = 0;
	}
}

public ham_KnifeDeployPost(wpn) 
{
    static id; id = get_weapon_ent_owner(wpn);
    
    if (!pev_valid(id) || !is_user_alive( id ) || !g_iKnife[ id ]) 
    	return;
    
    static WeaponID; WeaponID = cs_get_weapon_id(wpn); 
    
    static buffer[128];
    
    if(WeaponID == CSW_KNIFE)
    {
        formatex( buffer, charsmax( buffer ), "models/%s.mdl", szKnife_v);
        set_pev(id, pev_viewmodel2, buffer );
        
        formatex( buffer, charsmax( buffer ), "models/%s.mdl", szKnife_p);
        set_pev(id, pev_weaponmodel2, buffer);
    }
}
stock get_weapon_ent_owner(ent)
{
    if (pev_valid(ent) != 2)
        return -1;
    
    return get_pdata_cbase(ent, 41, 4);
} 