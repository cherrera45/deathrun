#include <amxmodx>
#include <cstrike>
#include <fun>
#include <hamsandwich>
#include <fakemeta>
#include <engine>

new PLUGIN[] = "DeathRun Frostnades"
new AUTHOR[] = "Schwabba"
new VERSION[] = "b1.0"

new g_trail, g_smoke, g_explosion

new g_mID, g_mID2
new g_manager[300][100];
new g_manager_name[300][100];
new bool:g_trapstate[9999]

new cvar_radius
new cvar_time

public plugin_precache()
{
	g_smoke = precache_model("sprites/steam1.spr");
	g_trail = precache_model("sprites/zbeam2.spr")
	g_explosion = precache_model("sprites/shockwave.spr");
}

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	cvar_radius = register_cvar("drfn_radius", "200.0")
	cvar_time = register_cvar("drfn_time", "10.0")
	RegisterHam(Ham_Think,"grenade","grenade_think",0)
	RegisterHam(Ham_Use, "func_button", "ButtonUsed", 0 );
	RegisterHam(Ham_Touch,"func_breakable","BreakableUsed")
}

public grenade_think(ent)
{
	static model[32]
	pev(ent, pev_model, model, charsmax(model))
	if(equal(model, "models/w_smokegrenade.mdl"))
	{
		set_rendering(ent, kRenderFxGlowShell, 0, 206, 209, kRenderNormal, 80)
		new Float:dmgtime;
		pev(ent,pev_dmgtime,dmgtime)
		if(dmgtime > get_gametime())
		{
			message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
			write_byte(TE_BEAMFOLLOW)
			write_short(ent)
			write_short(g_trail)
			write_byte(10)
			write_byte(5)
			write_byte(0)
			write_byte(206)
			write_byte(209)
			write_byte(192)
			message_end()
		}
		else
		{
			frostnade_explode(ent)
			return FMRES_SUPERCEDE
		}
	}
	
	return FMRES_IGNORED;
}

public frostnade_explode(ent)
{
	new id = pev(ent,pev_owner)
	new bool:temp_tf
	new Float:temp_origin[3]
	pev(ent,pev_origin,temp_origin)
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_SMOKE)
	write_coord(floatround(temp_origin[0]))
	write_coord(floatround(temp_origin[1]))
	write_coord(floatround(temp_origin[2]))
	write_short(g_smoke)
	write_byte(random_num(30,40))
	write_byte(5)
	message_end()
	create_blast(temp_origin)
	emit_sound(ent,CHAN_BODY,"warcraft3/frostnova.wav",VOL_NORM,ATTN_NORM,0,PITCH_NORM)
	new temp_around = -1;
	while((temp_around = find_ent_in_sphere(temp_around,temp_origin,get_pcvar_float(cvar_radius))) != 0)
	{
		new temp_target[32]
		pev(temp_around, pev_targetname, temp_target, charsmax(temp_target))
		new temp_class[32]
		pev(temp_around, pev_classname, temp_class, charsmax(temp_class))
		if(!equal(temp_class, "multi_manager"))
		{
			new temp_button = -1
			while((temp_button = find_ent_by_target(temp_button, temp_target)))
			{
				pev(temp_button, pev_classname, temp_class, charsmax(temp_class))
				if(equal(temp_class, "func_button"))
				{
					new temp_ent = -1
					while((temp_ent = find_ent_by_tname(temp_ent, temp_target)))
					{
						if(!g_trapstate[temp_ent])
						{
							temp_tf = true
							affect(temp_ent,temp_button)
						}
					}
				}
			}
			for(new i=0;i<g_mID;i++)
			{
				if(g_manager[i][1] && equal(temp_target, g_manager[i]))
				{
					new temp_button = -1
					while((temp_button = find_ent_by_target(temp_button, g_manager_name[i])))
					{
						pev(temp_button, pev_classname, temp_class, charsmax(temp_class))
						if(equal(temp_class, "func_button"))
						{
							new temp_ent = -1
							while((temp_ent = find_ent_by_tname(temp_ent, g_manager[i])))
							{
								if(!g_trapstate[temp_ent])
								{
									temp_tf = true
									affect(temp_ent,temp_button)
								}
							}
						}
					}
				}
			}
		}
		if(equal(temp_class, "func_breakable"))
		{
			if(!g_trapstate[temp_around])
			{
				new Float:temp_health
				pev(temp_around, pev_health, temp_health)
				if(temp_health >= 0.0 )  
				{
					temp_tf = true
					affect(temp_around,1)
				}
			}
		}
	}
	if(!temp_tf)
	{
		if(is_user_connected(id) && is_user_alive(id))
		{
			client_print(id, print_chat, "No target found, you get your nade back")
			new temp_ammo = cs_get_user_bpammo(id, CSW_SMOKEGRENADE)
			give_item(id, "weapon_smokegrenade")
			cs_set_user_bpammo(id, CSW_SMOKEGRENADE, temp_ammo +1)
		}
	}
	remove_entity(ent)
}

public affect(ent,button)
{
	if(button != 1)
	{
		g_trapstate[button] = true
	}
	g_trapstate[ent] = true
	new temp_trap[2]
	temp_trap[0] = ent
	temp_trap[1] = button
	set_task(get_pcvar_float(cvar_time), "enable", 0, temp_trap, 2)
	set_pev(ent, pev_renderfx, 0)
	set_pev(ent, pev_renderamt, 200.0)
	set_pev(ent, pev_rendercolor, 255.0, 0.0, 0.0)
	set_pev(ent, pev_rendermode, 1)
}

public enable(trap[])
{
	g_trapstate[trap[0]] = false
	g_trapstate[trap[1]] = false
	set_pev(trap[0], pev_renderfx, 0)
	set_pev(trap[0], pev_renderamt, 255.0)
	set_pev(trap[0], pev_rendercolor, 0.0, 0.0, 0.0)
	set_pev(trap[0], pev_rendermode, 0)
}

public ButtonUsed(button, id)
{
	if(g_trapstate[button])
	{
		client_print(id, print_chat, "This trap is currently disabled.")
		return HAM_SUPERCEDE
	}
	return HAM_IGNORED
}

public BreakableUsed(ent, id)
{
	if(g_trapstate[ent])
	{
		return HAM_SUPERCEDE
	}
	return HAM_IGNORED
}

create_blast(Float:origin[3])
{
	new temp_origin[3]
	FVecIVec(origin,temp_origin)

	new Float:temp_rgbF[3], temp_rgb[3]

	temp_rgbF[0] = 0.0
	temp_rgbF[1] = 206.0
	temp_rgbF[2] = 209.0

	FVecIVec(temp_rgbF,temp_rgb)

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2])
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2] + 385)
	write_short(g_explosion)
	write_byte(0)
	write_byte(0)
	write_byte(4)
	write_byte(60)
	write_byte(0)
	write_byte(temp_rgb[0])
	write_byte(temp_rgb[1])
	write_byte(temp_rgb[2])
	write_byte(100)
	write_byte(0)
	message_end()

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2])
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2] + 470)
	write_short(g_explosion)
	write_byte(0)
	write_byte(0)
	write_byte(4)
	write_byte(60)
	write_byte(0)
	write_byte(temp_rgb[0])
	write_byte(temp_rgb[1])
	write_byte(temp_rgb[2])
	write_byte(100)
	write_byte(0)
	message_end()

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2])
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2] + 555)
	write_short(g_explosion)
	write_byte(0)
	write_byte(0)
	write_byte(4)
	write_byte(60)
	write_byte(0)
	write_byte(temp_rgb[0])
	write_byte(temp_rgb[1])
	write_byte(temp_rgb[2])
	write_byte(100)
	write_byte(0)
	message_end()

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_DLIGHT)
	write_coord(temp_origin[0])
	write_coord(temp_origin[1])
	write_coord(temp_origin[2])
	write_byte(floatround(200/5.0))
	write_byte(temp_rgb[0])
	write_byte(temp_rgb[1])
	write_byte(temp_rgb[2])
	write_byte(8)
	write_byte(60)
	message_end()
}

public pfn_keyvalue(ent) 
{
	if(is_valid_ent(ent))
	{
		new temp_classname[33]
		new temp_keyname[33]
		new temp_keyvalue[33]
		copy_keyvalue(temp_classname,32,temp_keyname,32,temp_keyvalue,32)
		if(equal(temp_classname, "multi_manager"))
		{
			if(!equal(temp_keyname, "origin") && !equal(temp_keyname, "targetname") && !equal(temp_keyname, "spawnflags"))
			{
				g_mID++
				copy(g_manager[g_mID], 99, temp_keyname)
			}
			if(equal(temp_keyname, "targetname"))
			{
				for(new i;i<g_mID;i++)
				{
					if(g_mID2-1 < i)
					{
						g_mID2 = i+1
						copy(g_manager_name[g_mID2], 99, temp_keyvalue)
					}
				}
			}
		}
	}
}