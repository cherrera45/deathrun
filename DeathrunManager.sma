/***
	Deathrun Niveles by Hypnotize


	Ultima actualización

	08/02/2020

 */

#include < amxmodx >
#include < amxmisc >
//#include < reapi >
#include <cstrike>
#include <fun>
#include < engine >
#include < fakemeta >
#include < hamsandwich >
#include < sqlx >
#include < sys_cuentas >


#pragma semicolon 1

// Comment this line if you do not want to have fake player !
#define FAKE_PLAYER

// Bot name
#if defined FAKE_PLAYER
	new const g_szBotName[ ] = "DRM Fake Player";
#endif

new Ham:Ham_Player_ResetMaxSpeed = Ham_Item_PreFrame;

#define TASK_REMOVEMDL 1235
#define ID_RMVMDL ( taskid - TASK_REMOVEMDL )

// Messages prefix
new const g_szPrefix[ ] = "[ Deathrun ]";

// Global Variables
new bool:g_bHauntedHouse, bool:g_bGamerFun, bool:g_bRandromized, bool:g_bStarting, bool:g_bFirstRound;
new bool:g_bEnabled, bool:g_bRestart, bool:g_bConnected[ 33 ], g_iPuntos[ 33 ];
new g_szPlayerName[ 33 ][ 32 ], g_iMultiplicador[ 33 ], g_szTag[ 33 ][ 32 ];
new g_iTouch[ 33 ], g_id[ 33 ], szMapName[ 64 ], Float: g_fRecord[ 33 ], Float:g_fTiempo[ 33 ], kHasSpeed[ 33 ];

new g_pRemoveBuyZone, g_pHideHuds, g_pBlockMoney, g_pLifeSystem, g_pSvRestart, g_pAutoBalance, g_pLimitTeams;
new g_pNoFallDmg, g_pGameName, g_pToggle, g_pBlockSpray, g_pBlockRadio, g_pSemiclip, g_pGiveUsp, g_pBlockKill;
new g_iMsgHideWeapon, g_iMsgCrosshair, g_iMsgMoney, gmsg_SetFOV, g_iHudPrincipal, gCScore, gTScore, gCountRound;
new g_iMaxplayers, g_iHudSync, g_iHudSync2, g_iLastTerr, g_iThinker, g_msgTeamInfo, g_msgSayText;
new g_bButton[ 400 ], szRuta[ 84 ], g_iAccesorio[ 33 ], g_iTeamInvisible[ 33 ], g_iKills[ 33 ], g_iLlegadas[ 33 ];
new Float:g_PlayedTime[33];
new Float:g_pGameTime[33];

new Array:g_szItemName, Array:g_iItemNivel, Array:g_iItemPuntos, Array:g_iItemTipo, Array:g_iItemAdmin, fw_Item_Selected, g_iTotalItems, g_iTotalKnifes, g_iTotalUsps;
new Handle:g_hTuple;



enum
{
	KNIFE = 0,
	USP
};

enum 
{ 
	fNone = 0,
	fNormal, 
	fBunny, 
	fDroga,
	fGrav,
	fRespawn
};
enum eRango
{ 
	rango_name[ 120 ], 
	rango_level 
};

enum Perk_Struct 
{ 
	Perk_Tag[ 16 ], 
	Perk_Flag[ 22 ], 
	Perk_Ganancia 
};

enum
{
	REGISTRAR_USUARIO,
	LOGUEAR_USUARIO,
	GUARDAR_DATOS,
	CARGAR_ITEMS,
	COMPRAR_ITEMS,
	INSERTAR_RECORD,
	CARGAR_RECORD,
	UPDATEAR_RECORD,
	COMPRAR_HAT,
	CARGAR_HAT
};

enum STRUCT_ACCESORIOS 
{ 
	hat_name[64], 
	hat_model[64], 
	hat_acceso, 
	hat_nivel, 
	hat_precio 
};

enum hudd
{ 
	hudName[50], 
	hudColor[3] 
};

enum _:SHOPDATA 
{ 
	NOMBRE[ 50 ], 
	PRECIO 
};

enum eLogro
{
	nombre_logro[50],
	logro_requerido,
	logro_ganancia
};

const m_toggle_state = 41;
const LEVEL_MAX = 100;
const m_iVGUI = 510;

new const MYSQL_HOST[] = "127.0.0.1";
new const MYSQL_USER[] = "root";
new const MYSQL_PASS[] = "";
new const MYSQL_DATEBASE[] = "deathrun_world";

new const szTable[] = "dr_info";
new const g_szTableItems[] = "dr_items";
new const g_szTableRecord[] = "dr_mapas";
new const g_szTableHats[] = "dr_hats";

new const next_level[LEVEL_MAX] =
{
	10,
	30,
	60,
	90,
	120,
	150,
	200,
	250,
	300,
	350,
	400,
	500,
	600,
	700,
	800,
	1000,
	1200,
	1400,
	1600,
	1800,
	2000,
	2500,
	3000,
	3500,
	4000,
	4500,
	5100,
	5700,
	6300,
	6900,
	7500,
	8200,
	8900,
	9600,
	10300,
	11000,
	12000,
	13000,
	14000,
	15000,
	16000,
	17000,
	18000,
	19000,
	20000,
	21000,
	23000,
	25000,
	27000,
	29000,
	31000,
	34000,
	37000,
	40000,
	43000,
	46000,
	50000,
	54000,
	58000,
	62000,
	66000,
	71000,
	76000,
	81000,
	86000,
	91000,
	97000,
	103000,
	109000,
	115000,
	121000,
	128000,
	135000,
	142000,
	149000,
	156000,
	164000,
	175000,
	200000,
	200001
};

new const g_szRangos[ ][ eRango ] = 
{
	{ "Novato", 1 },
	{ "Aprendiz", 5 },
	{ "Salta Muros",  10 },
	{ "Kztero Noob", 15 },
	{ "Kztero Pro", 20 },
	{ "Kztero Lider", 25 },
	{ "Bunero", 30 },
	{ "Auto-Bunero", 35 },
	{ "Long Jumper", 40 },
	{ "High Jumper", 45  },
	{ "Duckero", 50 },
	{ "Scrolero", 55 },
	{ "Strafero Noob", 60 },
	{ "Strafero Pro", 65 },
	{ "Strafero Lider", 70 },
	{ "Maestro Parkour", 75 },
	{ "Mario Bros", 78 }
};

new const LLEGADAS[ ] [ eLogro ] =
{
	{ "Principiante", 1, 1 },
	{ "Acaba el mapa 75 veces", 75, 200 },
	{ "Acaba el mapa 125 veces", 125, 300 },
	{ "Acaba el mapa 275 veces", 275, 500 },
	{ "Intermedio 500", 500, 700 },
	{ "Acaba el mapa 600 veces", 600, 1000 },
	{ "Acaba el mapa 700 veces", 700, 1500 },
	{ "Acaba el mapa 800 veces", 800, 1800 },
	{ "El escapista 1k", 1000, 2100 }
};

new const MATADOS[ ] [ eLogro ] =
{
	{ "Principiante", 1, 1 },
	{ "Mata al TT 75 veces", 75, 200 },
	{ "Mata al TT 125 veces", 125, 300 },
	{ "Mata al TT 275 veces", 275, 500 },
	{ "Intermedio 500", 500, 700 },
	{ "Mata al TT 600 veces", 600, 1000 },
	{ "Mata al TT 700 veces", 700, 1500 },
	{ "Mata al TT 800 veces", 800, 1800 },
	{ "El MATADOR 1k", 1000, 2100 }
};

new const ACCESORIOS[ ][ STRUCT_ACCESORIOS ] =
{
	{ "skyrim",       "models/hats/10_MochilaPvZ.mdl",        -1, 1, 0 },
	{ "subzero",    "models/hats/angel.mdl",        -1, 5, 100 },
	{ "superspace",    "models/hats/crazycat.mdl",        ADMIN_BAN, 12, 1200 }
};

new const g_sPerks[ ][ Perk_Struct ] = 
{	
	{ "[ OWNER ]" , "abcdefghijklmnopqrstu", 4 },
    { "[ STAFF ]" , "abcdefijnopqrstu", 4 },
    { "[ GOLD ]" , "acdefijpqrstu", 3 },
	{ "[ VIP+ ]" , "cdefijnqrstu", 2 },
	{ "[ VIP ]" , "cefijmqrstu", 1 } 
};

new const g_ColorHud[][hudd] = {
	{"Blanco", {225, 225, 225}},
	{"Rojo", {225, 0, 0}},
	{"Verde", {0, 225, 0}},
	{"Azul", {0, 0, 225}},
	{ "Anaranjado", {255, 140, 0} },
    { "Blanco", {255, 255, 255} },
    { "Amarillo", {255, 255, 0} },
    { "Fucsia", {217, 0, 217} },
    { "Celeste",{ 0, 255, 255} }
};

new const Shop[][SHOPDATA] =
{
	{ "He granada", 10 },
	{ "Botas silenciosas", 10 },
	{ "+100 HP", 10 },
	{ "+200 Armadura", 10 }, 
	{ "Speed \d( Hasta morir )", 10 },
	{ "Gravedad \d( Hasta morir )", 10 },
	{ "Camuflaje \d( \y20 \dsegundos )", 10 },
	{ "Glow \d( \rColor al Azar \d)", 10 },
	{ "Respawn", 10 },
	{ "Smoke TRAP", 10 }
};

const Float:g_fTimeActived = 10.0;//tiempo para dar FR
const TASK_CONTEO = 5555;
new g_iCount;
new cvar_timeFr;

new g_iFreeRound, Float:g_fTimeFreeActived;

new g_iSemiClip[ 33 ], g_Lifes[ 33 ], g_iHud[33], g_ieKills[ 33 ], g_ieLlegadas[ 33 ];
new g_iLevel[ 33 ], g_iExp[ 33 ], g_iRango[ 33 ], g_iItem[ 33 ][ 2 ];

#if defined FAKE_PLAYER
	new g_iFakeplayer;
#endif

// Macros
#if cellbits == 32
	#define OFFSET_BZ 235
#else
	#define OFFSET_BZ 268
#endif

#if AMXX_VERSION_NUM > 182
	#define client_disconnect client_disconnected
#else
	#include <dhudmessage>
#endif

new const g_szWinCT[][] = { "deathrun_world/wincts.wat" };
new const g_szWinTT[][] = { "deathrun_world/wintts.wat" };
new const g_szModelButton[] = "models/deathrun_dg/modelo_boton.mdl";
new const g_iEnt[] = "ZonaSegura";
new const ent_cm[] = "hat_ent";

new g_iStatus[33];
enum
{
	NO_LOGUEADO = 0,
	LOGUEADO
}

// =======================================================================================

public user_login_post( id, iUserid )
{
	if( is_registered( id ) )
	{
		if( is_user_connected( id ) )
		{
			new szQuery[ 128 ], iData[ 2 ];
			g_id[ id ] = iUserid;
			iData[ 0 ] = id;
			iData[ 1 ] = LOGUEAR_USUARIO;

			formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE id_cuenta='%d'", szTable, g_id[ id ] );
			SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
			
		}
	}
}

public plugin_precache( )
{
	new i;

	for( i = 0; i < sizeof( g_szWinTT ) ; ++i )
		fnPrecacheSound( g_szWinTT[ i ] );
	
	for( i = 0; i < sizeof( g_szWinCT ) ; ++i )
		fnPrecacheSound( g_szWinCT[ i ] );

	for( i = 0; i < sizeof( ACCESORIOS ); i++ )
		precache_model( ACCESORIOS[ i ][ hat_model ] );
	
	precache_model(g_szModelButton);
}

public plugin_natives()
{
	register_native("register_item", "native_item", 0);
	register_native("get_points", "native_getpoints", 1);
	register_native("set_points", "native_setpoints", 1);

}
public native_getpoints(id) return g_iPuntos[ id ];
public native_setpoints(id, cant)
{
	g_iPuntos[ id ] = cant;
	return cant;
}

public plugin_init( ) {
	new const VERSION[ ] = "3.0.3";
	
	register_plugin( "Deathrun Manager", VERSION, "xPaw & Hypnotize - http://divstarproject.com/" );
	
	g_pToggle        = register_cvar( "deathrun_toggle",     "1" );
	g_pBlockSpray    = register_cvar( "deathrun_spray",      "1" );
	g_pBlockRadio    = register_cvar( "deathrun_radio",      "1" );
	g_pBlockKill     = register_cvar( "deathrun_blockkill",  "1" );
	g_pBlockMoney    = register_cvar( "deathrun_blockmoney", "1" );
	g_pSemiclip      = register_cvar( "deathrun_semiclip",   "1" );
	g_pGiveUsp       = register_cvar( "deathrun_giveusp",    "1" );
	g_pHideHuds      = register_cvar( "deathrun_hidehud",    "1" );
	g_pLifeSystem    = register_cvar( "deathrun_lifesystem", "1" );
	g_pGameName      = register_cvar( "deathrun_gamename",   "1" );
	g_pNoFallDmg     = register_cvar( "deathrun_terrnfd",    "1" );
	g_pRemoveBuyZone = register_cvar( "deathrun_removebz",   "1" );
	cvar_timeFr = register_cvar( "deathrun_timefree_round",   "60" );

	// Lets get map name...
	static szPath[ 64 ];
	get_mapname( szMapName, 63 );
	get_configsdir(szPath, charsmax(szPath));
	formatex(szRuta, charsmax(szRuta), "%s/%s.ini", szPath, szMapName);

	if( contain( szMapName, "deathrun_" ) != -1 ) 
	{
		set_pcvar_num( g_pToggle, 1 );
		
		if( contain( szMapName, "hauntedhouse" ) != -1 )
			g_bHauntedHouse = true;
		else {
			g_bHauntedHouse = false;
			
			if( equal( szMapName, "deathrun_gamerfun" ) )
				g_bGamerFun = true;
			else
				g_bGamerFun = false;
		}
	} else
		set_pcvar_num( g_pToggle, 0 );
	
	g_pSvRestart   = get_cvar_pointer( "sv_restart" );
	g_pAutoBalance = get_cvar_pointer( "mp_autoteambalance" );
	g_pLimitTeams  = get_cvar_pointer( "mp_limitteams" );
	
	register_cvar( "deathrun_version", VERSION, FCVAR_SERVER | FCVAR_SPONLY );
	set_cvar_string( "deathrun_version", VERSION );
	set_cvar_num("mp_freezetime ", 0);
	
	// Registering Language file
	register_dictionary( "deathrun.txt" );

	fw_Item_Selected = CreateMultiForward("dr_item", ET_STOP, FP_CELL, FP_CELL, FP_CELL);

	g_szItemName = ArrayCreate(32);
	g_iItemNivel = ArrayCreate();
	g_iItemPuntos = ArrayCreate();
	g_iItemTipo = ArrayCreate();
	g_iItemAdmin = ArrayCreate();
	
	// Logging Events
	register_logevent( "EventRoundStart", 2, "1=Round_Start" );
	register_logevent( "EventRandromize", 2, "1=Round_End" );
	register_event( "SendAudio", "EventTerrsWin",   "a", "2&%!MRAD_terwin" );
	register_event( "TextMsg",	 "EventRandromize", "a", "2&#Game_w" );
	register_event( "DeathMsg",	 "EventDeath",      "a");
	register_event( "Money",	 "EventMoney",      "b" );
	register_event( "ResetHUD",	 "EventResetHud",   "be" );
	register_event( "CurWeapon", "EventCurWeapon", "be", "1=1", "2!29" );
	register_event( "TextMsg","event_GameRestart","a","2&#Game_w" );

	register_clcmd("say", "clcmd_say");
	register_clcmd("say_team", "clcmd_say");
	register_clcmd( "say /free",      "fnFreeRound" );
	register_clcmd( "say fr",      "fnFreeRound" );
	register_clcmd( "say /fr",      "fnFreeRound" );
	register_clcmd( "say free",      "fnFreeRound" );
	register_clcmd( "say_team /free",      "fnFreeRound" );
	register_clcmd( "say_team fr",      "fnFreeRound" );
	register_clcmd( "say_team /fr",      "fnFreeRound" );
	register_clcmd( "say_team free",      "fnFreeRound" );
	register_clcmd("chooseteam", "clcmd_changeteam");
	register_clcmd("say record", "cmdRecord");
	register_clcmd("say /record", "cmdRecord");
	register_clcmd("say_team record", "cmdRecord");
	register_clcmd("say_team /record", "cmdRecord");
	register_clcmd( "say /shop", "clcmdSHOP" );
	register_clcmd( "say /tienda", "clcmdSHOP" );
	register_clcmd( "say /time", "cmdTime" );

	g_bFirstRound    = true;
	g_iMaxplayers    = get_maxplayers( );
	g_iMsgHideWeapon = get_user_msgid( "HideWeapon" );
	g_iMsgCrosshair  = get_user_msgid( "Crosshair" );
	g_iMsgMoney      = get_user_msgid( "Money" );
	gmsg_SetFOV 	 = get_user_msgid("SetFOV");
	g_msgTeamInfo 	 = get_user_msgid("TeamInfo");
	g_msgSayText 	 = get_user_msgid("SayText");
	g_iHudSync       = CreateHudSyncObj( );
	g_iHudSync2      = CreateHudSyncObj( );

	RegisterHam(Ham_Player_Jump, "player", "FW_Player_Jump");
	//RegisterHookChain(RG_CBasePlayer_Jump, "FW_Player_Jump", true);
	RegisterHam(Ham_Use, 	"func_button", 		"ham_Use_Button");
	RegisterHam(Ham_Use, 	"func_rot_button", 	"ham_Use_Button");
	RegisterHam(Ham_Use, 	"button_target", 	"ham_Use_Button");
	RegisterHam(Ham_Killed, "player", "fw_Killed");
	RegisterHam( Ham_Player_ResetMaxSpeed, "player", "ham_ResetMaxSpeedPost", true);

 	register_forward(FM_AddToFullPack, "Fw_AddToFullPack", true);

	if( get_pcvar_num( g_pToggle ) ) 
	{
		RegisterHam( Ham_TakeDamage, "player", "FwdHamPlayerDamage" );
		RegisterHam( Ham_Spawn,      "player", "FwdHamPlayerSpawn", 1 );
		register_forward( FM_ClientKill,       "FwdClientKill" );
		register_impulse( 201, "FwdImpulse_201" );
		
		if( get_pcvar_num( g_pGameName ) )
			register_forward( FM_GetGameDescription, "FwdGameDesc" );
		
		register_clcmd( "say /lifes", "CmdShowlifes" );
		register_clcmd( "say /lives", "CmdShowlifes" );
		
		register_clcmd( "radio1", "CmdRadio" );
		register_clcmd( "radio2", "CmdRadio" );
		register_clcmd( "radio3", "CmdRadio" );
		
		// Terrorist Check
		g_iThinker = create_entity( "info_target" );
		//g_iThinker = rg_create_entity("info_target");
		
		if( is_valid_ent( g_iThinker ) )
		{
			entity_set_string( g_iThinker, EV_SZ_classname, "DeathrunThinker" );
			//get_entvar(g_iThinker, var_classname, "DeathrunThinker");
			entity_set_float( g_iThinker, EV_FL_nextthink, get_gametime( ) + 20.0 );
			//get_entvar(g_iThinker, var_nextthink, get_gametime( ) + 20.0 );
			g_bRestart = true;
			
			// First think will happen in 20.0, Restart will be done there.
			
			register_think( "DeathrunThinker", "FwdThinker" );
		} else 
		{
			set_task( 15.0, "CheckTerrorists", _, _, _, "b" );
			
			// Lets make restart after 20 seconds from map start.
			set_task( 20.0, "RestartRound" );
		}
		
		if( get_pcvar_num( g_pRemoveBuyZone ) ) 
		{
			register_message( get_user_msgid( "StatusIcon" ), "MsgStatusIcon" ); // BuyZone Icon
			
			// Remove buyzone on map
			remove_entity_name( "info_map_parameters" );

			remove_entity_name( "func_buyzone" );
			
			// Create own entity to block buying
			new iEntity = create_entity( "info_map_parameters" );
			
			DispatchKeyValue( iEntity, "buying", "3" );
			DispatchSpawn( iEntity );
		}
		
		if( get_pcvar_num( g_pSemiclip ) ) 
		{
			register_forward( FM_StartFrame,	"FwdStartFrame", 0 );
			register_forward( FM_AddToFullPack,	"FwdFullPack",   1 );
		}
		
		g_bEnabled = true;
		
	#if defined FAKE_PLAYER
		new iEntity, iCount;
		
		while( ( iEntity = find_ent_by_class( iEntity, "info_player_deathmatch" ) ) > 0 )
			if( iCount++ > 1 )
				break;
		
		if( iCount <= 1 )
			g_iFakeplayer = -1;
		
		set_task( 5.0, "UpdateBot" );
		
		register_message( get_user_msgid( "DeathMsg" ), "MsgDeathMsg" );
	#endif
	} else
		g_bEnabled = false;

	g_iHudPrincipal = CreateHudSyncObj( );

	new ent;
	for (new i = 1; i <= 32; i++)
	{
		ent = create_entity("info_target");
		entity_set_int(ent, EV_INT_movetype, MOVETYPE_FOLLOW);
		entity_set_edict(ent, EV_ENT_owner, i);
		entity_set_string(ent, EV_SZ_classname, ent_cm);
	}

	MySQL_Init();
	ReadEntity();
}
public MySQL_Init()
{
	g_hTuple = SQL_MakeDbTuple( MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DATEBASE );
	
	if( !g_hTuple ) 
	{
		log_to_file( "SQL_ERROR.txt", "No se pudo conectar con la base de datos." );
		return pause( "a" );
	}

	return PLUGIN_CONTINUE;
}
public plugin_end( )
	SQL_FreeHandle( g_hTuple ); 
// FAKEPLAYER
///////////////////////////////////////////
#if defined FAKE_PLAYER
	public UpdateBot( ) {
		if( g_iFakeplayer == -1 )
			return;
		
		new id = find_player( "i" );
		
		if( !id ) {
			id = engfunc( EngFunc_CreateFakeClient, g_szBotName );
			if( pev_valid( id ) ) {
				engfunc( EngFunc_FreeEntPrivateData, id );
				dllfunc( MetaFunc_CallGameEntity, "player", id );
				set_user_info( id, "rate", "3500" );
				set_user_info( id, "cl_updaterate", "25" );
				set_user_info( id, "cl_lw", "1" );
				set_user_info( id, "cl_lc", "1" );
				set_user_info( id, "cl_dlmax", "128" );
				set_user_info( id, "cl_righthand", "1" );
				set_user_info( id, "_vgui_menus", "0" );
				set_user_info( id, "_ah", "0" );
				set_user_info( id, "dm", "0" );
				set_user_info( id, "tracker", "0" );
				set_user_info( id, "friends", "0" );
				set_user_info( id, "*bot", "1" );
				set_pev( id, pev_flags, pev( id, pev_flags ) | FL_FAKECLIENT );
				set_pev( id, pev_colormap, id );
				
				new szMsg[ 128 ];
				dllfunc( DLLFunc_ClientConnect, id, g_szBotName, "127.0.0.1", szMsg );
				dllfunc( DLLFunc_ClientPutInServer, id );
				
				//rg_set_user_team( id, CS_TEAM_T );
				cs_set_user_team(id, CS_TEAM_T);
				ExecuteHamB( Ham_CS_RoundRespawn, id );
				
				set_pev( id, pev_effects, pev( id, pev_effects ) | EF_NODRAW );
				set_pev( id, pev_solid, SOLID_NOT );
				dllfunc( DLLFunc_Think, id );
				
				g_iFakeplayer = id;
			}
		}
	}
	
	public MsgDeathMsg( const iMsgId, const iMsgDest, const id ) {
		if( get_msg_arg_int( 2 ) == g_iFakeplayer )
			return PLUGIN_HANDLED;
		
		return PLUGIN_CONTINUE;
	}
#endif
public Fw_AddToFullPack( es, e, ent, host, flags, player, set )
{
	if ( player && g_iTeamInvisible[ player ] )
	{
		if ( host != ent )
		{
			if ( get_pdata_int ( host, 114, 5 ) == get_pdata_int ( ent, 114, 5 ) )
			{
				set_es ( es, ES_RenderFx, kRenderFxGlowShell );
				set_es ( es, ES_RenderColor, { 0, 0, 255 } );
				set_es ( es, ES_RenderMode, kRenderTransTexture );
				set_es ( es, ES_RenderAmt, 1 );
			}
		}
	}
} 
// NEW TERRORIST
///////////////////////////////////////////
public EventRandromize( ) {
	if( !g_bEnabled || g_bFirstRound || g_bRandromized )
		return PLUGIN_CONTINUE;
	
	g_bRandromized = true;
	
	new i, iPlayers[ 32 ], iNum, iPlayer;
	get_players( iPlayers, iNum, "c" );
	
	if( iNum <= 1 )
		return PLUGIN_CONTINUE;
	
	for( i = 0; i < iNum; i++ ) {
		iPlayer = iPlayers[ i ];
		
		if( cs_get_user_team(iPlayer) == CS_TEAM_T )
			cs_set_user_team( iPlayer, CS_TEAM_CT );
	}
	
	new iRandomPlayer, CsTeams:iTeam;
	
	while( ( iRandomPlayer = iPlayers[ random_num( 0, iNum - 1 ) ] ) == g_iLastTerr ) { }
	
	g_iLastTerr = iRandomPlayer;
	
	iTeam = cs_get_user_team(iRandomPlayer);
	
	if( iTeam == CS_TEAM_T || iTeam == CS_TEAM_CT ) {
		cs_set_user_team(iRandomPlayer, CS_TEAM_T);
		
		new szName[ 32 ];
		get_user_name( iRandomPlayer, szName, 31 );
		
		for( i = 0; i < iNum; i++ )
			client_print_color( iPlayers[ i ], print_team_red, "%s^4 %L", g_szPrefix, iPlayers[ i ], "DR_NOW_TERR", szName);
		if( !g_bRestart && is_valid_ent( g_iThinker ) )
			entity_set_float( g_iThinker, EV_FL_nextthink, get_gametime( ) + 15.0 );
	} else {
		g_bRandromized = false;
		EventRandromize( );
	}
	
	if( fnGetAliveTeam(CS_TEAM_CT) >= fnGetAliveTeam(CS_TEAM_T) )
	{
		set_Msg( 0, "fue ganada por mario Team", g_szWinCT[ random_num( 0, charsmax( g_szWinCT ) ) ] );
		++gCScore;
	}
	else
	{
		set_Msg( 0, "fue ganada por los Bowser!", g_szWinTT[ random_num( 0, charsmax( g_szWinTT )-1 ) ] );
		++gTScore;
	}

	return PLUGIN_CONTINUE;
}
public event_GameRestart( ) 
{
	gCountRound = gTScore = gCScore = 0;
}
// NEW ROUND
///////////////////////////////////////////
public EventRoundStart( ) 
{
	if( !g_bEnabled )
		return PLUGIN_CONTINUE;

	g_iCount = get_pcvar_num(cvar_timeFr);
	remove_task(TASK_CONTEO);

	for(new i=1;i<= MAX_PLAYERS ; ++i)
	{
		if(!is_user_connected(i) || !is_logged(i) || i == g_iFakeplayer || g_iStatus[ i ] != LOGUEADO)
			continue;

		message_begin( MSG_ALL, gmsg_SetFOV, { 0, 0, 0 }, i );
		write_byte( 0 );
		message_end( ) ;

		guardar_datos(i);

		if(is_user_admin(i) && g_Lifes[i] <= 0)
		{
			g_Lifes[i]++;
			client_print_color(i, print_team_blue, "Se te otorgo una vida extra por ser admin.");
		}
		g_fTiempo[ i ] = 0.0;
	}
	
	arrayset(g_iTouch, 0, sizeof(g_iTouch));
	arrayset(g_bButton, false, charsmax(g_bButton));

	g_bRandromized	= false;
	g_bStarting	= false;
	g_iFreeRound = fNone;
	g_fTimeFreeActived = get_gametime() + g_fTimeActived;

	new i, iPlayers[ 32 ], iNum, iRealPlayers, CsTeams:iTeam;
	get_players( iPlayers, iNum, "c" );
	
	if( iNum <= 1 )
		return PLUGIN_CONTINUE;
	
	for( i = 0; i < iNum; i++ ) {
		iTeam = cs_get_user_team( iPlayers[ i ] );
		
		if( iTeam == CS_TEAM_T || iTeam == CS_TEAM_CT )
			iRealPlayers++;
	}
	
	if( iRealPlayers <= 1 ) {
		set_hudmessage( 0, 128, 0, -1.0, 0.1, 0, 4.0, 4.0, 0.5, 0.5, 4 );
		
		for( i = 0; i < iNum; i++ )
			ShowSyncHudMsg( iPlayers[ i ], g_iHudSync, "%L", iPlayers[ i ], "DR_NOT_ENOUGH" );
		
		return PLUGIN_CONTINUE;
	}
	
	set_pcvar_num( g_pAutoBalance, 0 );
	set_pcvar_num( g_pLimitTeams, 0 );
	
	if( g_bFirstRound ) {
		set_hudmessage( 0, 128, 0, -1.0, 0.1, 0, 4.0, 4.0, 0.5, 0.5, 4 );
		
		for( i = 0; i < iNum; i++ ) {
			ShowSyncHudMsg( iPlayers[ i ], g_iHudSync, "%L", iPlayers[ i ], "DR_STARTING" );
			
			client_print_color( iPlayers[ i ], print_team_red, "%s^1 %L", g_szPrefix, iPlayers[ i ], "DR_STARTING_CC" );
		}
		
		if( is_valid_ent( g_iThinker ) ) {
			g_bRestart = true;
			
			entity_set_float( g_iThinker, EV_FL_nextthink, get_gametime( ) + 9.0 );
		} else
			set_task( 9.0, "RestartRound" );
		
		g_bStarting = true;
		g_bFirstRound = false;
	}
	
	return PLUGIN_CONTINUE;
}

// CHECK TERRORIST
///////////////////////////////////////////
public FwdThinker( iEntity ) {
	if( g_bRestart ) {
		g_bRestart = false;
		
		RestartRound( );
	} else
		CheckTerrorists( );
	
	entity_set_float( iEntity, EV_FL_nextthink, get_gametime( ) + 15.0 );
}

public CheckTerrorists( ) {
	if( !g_bEnabled || g_bFirstRound || g_bStarting )
		return PLUGIN_CONTINUE;
	
	new i, iPlayers[ 32 ], iTerrors, iNum, iRealPlayers, CsTeams:iTeam;
	get_players( iPlayers, iNum, "c" );
	
	if( iNum <= 1 )
		return PLUGIN_CONTINUE;
	
	for( i = 0; i < iNum; i++ ) {
		iTeam = cs_get_user_team( iPlayers[ i ] );
		
		if( iTeam == CS_TEAM_T )
			iTerrors++;
		
		if( iTeam == CS_TEAM_T || iTeam == CS_TEAM_CT )
			iRealPlayers++;
	}
	
	if( iRealPlayers <= 1 ) {
		set_hudmessage( 0, 128, 0, -1.0, 0.1, 0, 4.0, 4.0, 0.5, 0.5, 4 );
		
		for( i = 0; i < iNum; i++ )
			ShowSyncHudMsg( iPlayers[ i ], g_iHudSync, "%L", iPlayers[ i ], "DR_NOT_ENOUGH" );
		
		return PLUGIN_CONTINUE;
	}
	
	if( iTerrors == 0 ) {
		for( i = 0; i < iNum; i++ ) {
			client_print_color( iPlayers[ i ], print_team_red, "%s^1 %L", g_szPrefix, iPlayers[ i ], "DR_NO_DETECT");
			
			if( is_user_alive( iPlayers[ i ] ) && cs_get_user_team( iPlayers[ i ] ) == CS_TEAM_CT )
				user_silentkill( iPlayers[ i ] );
		}
		
		set_task( 0.5, "EventRandromize" );
	}
	
	return PLUGIN_CONTINUE;
}

// LIFE SYSTEM
///////////////////////////////////////////
public EventTerrsWin( ) {
	if( !g_bEnabled || g_bFirstRound )
		return PLUGIN_CONTINUE;
	
	new iPlayers[ 32 ], iNum, iPlayer;
	get_players( iPlayers, iNum, "c" );
	
	if( iNum <= 1 )
		return PLUGIN_CONTINUE;
	
	new iLifeCvar = get_pcvar_num( g_pLifeSystem );
	
	for( new i = 0; i < iNum; i++ ) {
		iPlayer = iPlayers[ i ];
		
		if( cs_get_user_team( iPlayer ) == CS_TEAM_T ) {
			//set_entvar(iPlayer, var_frags, get_entvar(iPlayer, var_frags) + 3);
			set_user_frags( iPlayer, get_user_frags( iPlayer ) + 3 );
			
			if( iLifeCvar == 2 )
				g_Lifes[ iPlayer ]++;
		}
	}
	
	return PLUGIN_CONTINUE;
}

public EventDeath( ) {
	if( !g_bEnabled )
		return PLUGIN_CONTINUE;
	
#if defined FAKE_PLAYER
	new iVictim = read_data( 2 );
	new iTeam = get_user_team( iVictim );
	
	if( iTeam == 1 && is_user_alive( g_iFakeplayer ) )
		fakedamage( g_iFakeplayer, "worldspawn", 100.0, DMG_GENERIC );
	
	if( !get_pcvar_num( g_pLifeSystem ) )
		return PLUGIN_CONTINUE;
#else
	if( !get_pcvar_num( g_pLifeSystem ) )
		return PLUGIN_CONTINUE;
	
	new iVictim = read_data( 2 );
	new iTeam = get_user_team( iVictim );
#endif
	
	new iKiller = read_data( 1 );
	
	if( iKiller != iVictim && get_user_team(iKiller) != iTeam )
		g_Lifes[iKiller]++;
	
	if( cs_get_user_team( iVictim ) == CS_TEAM_CT && g_Lifes[ iVictim ] > 0 ) 
	{
		static iCTcount; iCTcount = fnGetAliveTeam(CS_TEAM_CT);
		
		if( iCTcount > 1 ) 
		{
			set_task(3.2, "menu_Lifes", iVictim);
			
			client_print_color( iVictim, print_team_red, "%s^1 %L", g_szPrefix, iVictim, "DR_LIFE_RESPAWN" );
		}
	}
	
	return PLUGIN_CONTINUE;
}
public fnRevivePlayer( id ) 
{
	if( !g_bConnected[ id ] || cs_get_user_team( id ) != CS_TEAM_CT ) 
		return;

	
	static iCTcount; iCTcount = fnGetAliveTeam(CS_TEAM_CT);

	if( iCTcount > 1 ) 
	{
		ExecuteHamB( Ham_CS_RoundRespawn, id );
		g_Lifes[ id ]--;
	}
}

public CmdShowlifes( id ) {
	if( get_pcvar_num( g_pLifeSystem ) == 0 ) {
		client_print_color( id, print_team_red, "%s^1 %L", g_szPrefix, id, "DR_LIFE_DISABLE" );
		return PLUGIN_HANDLED;
	}
	
	if( g_Lifes[ id ] > 0 )
		client_print_color( id, print_team_red, "%s^1 %L", g_szPrefix, id, "DR_LIFE_CC_COUNT", g_Lifes[ id ] );
	else
		client_print_color( id, print_team_red, "%s^1 %L", g_szPrefix, id, "DR_LIFE_CC_NO" );
	
	return PLUGIN_HANDLED;
}

public Showlifes( id ) {
	set_hudmessage( 0, 128, 0, 0.04, 0.71, 0, 2.5, 2.5, 0.5, 0.5, 3 );
	
	if( g_Lifes[ id ] > 0 )
		ShowSyncHudMsg( id, g_iHudSync2, "%L", id, "DR_LIFE_COUNT", g_Lifes[ id ] );
	else
		ShowSyncHudMsg( id, g_iHudSync2, "%L", id, "DR_LIFE_NO" );
}

// EVENTS
///////////////////////////////////////////
public EventResetHud( id ) {
	if( g_bEnabled && get_pcvar_num( g_pHideHuds ) && !is_user_bot( id ) ) {
		message_begin( MSG_ONE_UNRELIABLE, g_iMsgHideWeapon, _, id );
		write_byte( ( 1<<4 | 1<<5 ) );
		message_end( );
		
		message_begin( MSG_ONE_UNRELIABLE, g_iMsgCrosshair, _, id );
		write_byte( 0 );
		message_end( );
	}
}

public fw_Killed( victim, attacker, shouldgib ) 
{
	if( attacker == victim || !is_user_connected( attacker ) || !is_user_connected( victim ) ) 
		return;
	
	static exp; exp = 10;
	
	fnCheckLevel( attacker, exp, 15 );
} 

public EventMoney( id ) {
	if( g_bEnabled && get_pcvar_num( g_pBlockMoney ) ) {
		set_pdata_int( id, 115, 0 );
		
		message_begin( MSG_ONE_UNRELIABLE, g_iMsgMoney, _, id );
		write_long ( 0 );
		write_byte ( 1 );
		message_end( );
	}
}

public client_putinserver( id )
{
	new i;

	get_user_name( id, g_szPlayerName[ id ], charsmax( g_szPlayerName[ ] ) );

	g_szTag[ id ][ 0 ] = EOS;
	g_iMultiplicador[ id ] = 1;

	for( i = 0 ; i < sizeof g_sPerks; ++i )
	{
		if( has_all_flags( id , g_sPerks[ i ][ Perk_Tag ] ) )
		{
			copy( g_szTag[ id ] , 31 , g_sPerks[ i ][ Perk_Tag ] );
			g_iMultiplicador[ id ] = g_sPerks[ i ][ Perk_Ganancia ];
			break;
		}
	}
	g_iStatus[id] = NO_LOGUEADO;
	g_bConnected[ id ] = true;
	kHasSpeed[ id ] = false;
	g_iLevel[ id ] = 1;
	g_iAccesorio[ id ] = -1;
	g_ieKills[ id ] =  g_ieLlegadas[ id ] = g_iTeamInvisible[ id ] = g_iHud[ id ] = g_iPuntos[ id ] = g_iExp[ id ] = g_iRango[ id ] = 0;
	g_iItem[ id ][ USP ] = g_iItem[ id ][ KNIFE ] = -1;
	g_fRecord[ id ] = 999.99;
	g_fTiempo[ id ] = 0.0;

	g_pGameTime[id] = get_gametime();
}

public client_disconnect( id ) 
{
	remove_task( id + TASK_REMOVEMDL );

	if (g_iAccesorio[id] >= 0)
	{
		new ent = find_ent_by_owner(-1, ent_cm, id);
		entity_set_model(ent, "");
		entity_set_edict(ent, EV_ENT_aiment, 0);
		entity_set_int(ent, EV_INT_movetype, MOVETYPE_NONE);
		entity_set_origin(ent, Float:{8192.0,8192.0,8192.0});

		g_iAccesorio[id] = -1;
	}
	if(is_logged(id) && g_iStatus[ id ] == LOGUEADO)
	{
		guardar_datos(id);
	}


	remove_task( id+7894 );

	g_bConnected[ id ] = false;
	CheckTerrorists( );

	if( !g_bRestart && is_valid_ent( g_iThinker ) )
		entity_set_float( g_iThinker, EV_FL_nextthink, get_gametime( ) + 15.0 );
	
#if defined FAKE_PLAYER
	if( g_iFakeplayer == id ) 
	{
		set_task( 1.5, "UpdateBot" );
		
		g_iFakeplayer = 0;
	}
#endif
}
public ham_ResetMaxSpeedPost(index)
{
	if(!is_user_alive(index))
		return;

	set_player_speed(index);
} 

public setItem(id)
{
	if( g_iTotalKnifes > 0 && g_iItem[id][KNIFE] >= 0 ) give_api(id, g_iItem[ id ][ KNIFE ], KNIFE);
	if( g_iTotalUsps > 0 && g_iItem[id][USP] >= 0 ) give_api(id, g_iItem[ id ][ USP ], USP);
	//else if( get_pcvar_num( g_pGiveUsp ) && iTeam == CS_TEAM_CT && !g_bHauntedHouse ) GiveUsp(id);
}

set_player_speed(index) 
{
	if(!is_user_alive( index ) ) return;
	
	if( kHasSpeed[ index ] )
		set_pev(index, pev_maxspeed, 290.0 );
	else
		set_pev(index, pev_maxspeed, 245.0 );
	
}

public RemoveMdl( taskid )
{
	if(!is_user_alive( ID_RMVMDL ) ) return;

	cs_set_user_model( ID_RMVMDL, "sas" );
}
// SEMICLIP
///////////////////////////////////////////
public FwdFullPack( es, e, ent, host, flags, player, pSet ) {
	if( !g_bEnabled )
		return FMRES_IGNORED;
	
	if( player && g_iSemiClip[ ent ] && g_iSemiClip[ host ] ) {
		set_es( es, ES_Solid, SOLID_NOT );
		set_es( es, ES_RenderMode, kRenderTransAlpha );
		set_es( es, ES_RenderAmt, 85 );
	}
	
	return FMRES_IGNORED;
}

public FwdStartFrame( ) {
	if( !g_bEnabled )
		return FMRES_IGNORED;
	
	static iPlayers[ 32 ], iNum, iPlayer, iPlayer2, i, j;
	get_players( iPlayers, iNum, "ache", "CT" );
	
	arrayset( g_iSemiClip, 0, 32 );
	
	if( iNum <= 1 )
		return FMRES_IGNORED;
	
	for( i = 0; i < iNum; i++ ) {
		iPlayer = iPlayers[ i ];
		
		for( j = 0; j < iNum; j++ ) {
			iPlayer2 = iPlayers[ j ];
			
			if( iPlayer == iPlayer2 )
				continue;
			
			if( g_iSemiClip[ iPlayer ] && g_iSemiClip[ iPlayer2 ] )
				continue;
			
			if( entity_range( iPlayer, iPlayer2 ) < 128 ) {
				g_iSemiClip[ iPlayer ]	= true;
				g_iSemiClip[ iPlayer2 ]	= true;
			}
		}
	}
	
	for( i = 0; i < iNum; i++ ) {
		iPlayer = iPlayers[ i ];
		
		set_pev( iPlayer, pev_solid, g_iSemiClip[ iPlayer ] ? SOLID_NOT : SOLID_SLIDEBOX );
	}
	
	return FMRES_IGNORED;
}

// FORWARDS
///////////////////////////////////////////
public FwdHamPlayerSpawn( id ) {
	if( !g_bEnabled || !is_user_alive( id ) )
		return HAM_IGNORED;
	
	if( get_pcvar_num( g_pBlockRadio ) ) // thanks to ConnorMcLeod for this good way :)
		set_pdata_int( id, 192, 0 );
	
#if defined FAKE_PLAYER
	if( g_iFakeplayer == id ) {
		set_pev( id, pev_frags, -1000.0 );
		//set_entvar( id, var_frags, -1000 );
		cs_set_user_deaths( id, -1000 );
		
		set_pev( id, pev_effects, pev( id, pev_effects ) | EF_NODRAW );
		set_pev( id, pev_solid, SOLID_NOT );
		entity_set_origin( id, Float:{ 999999.0, 999999.0, 999999.0 } );
		dllfunc( DLLFunc_Think, id );
	} else {
#endif
		new CsTeams:iTeam = cs_get_user_team( id );
		
		// An small delay for message
		if( get_pcvar_num( g_pLifeSystem ) != 0 && iTeam == CS_TEAM_CT )
			set_task( 0.8, "Showlifes", id );
		
		strip_user_weapons( id );

		give_item( id, "weapon_knife" );
		
		set_pdata_int( id, 116, 0 ); // Pickup fix by ConnorMcLeod
		
		if( g_bGamerFun && iTeam == CS_TEAM_CT )
			give_item( id, "weapon_smokegrenade" );
		
		/*if( get_pcvar_num( g_pGiveUsp ) && iTeam == CS_TEAM_CT && !g_bHauntedHouse )
			set_task( 1.0, "GiveUsp", id );*/

		set_user_rendering( id );

		if( iTeam == CS_TEAM_CT )
			set_task(1.0, "setItem", id);
		

#if defined FAKE_PLAYER
	}
#endif
	
	return HAM_IGNORED;
}
/*
public GiveUsp( const id ) 
{
	if( is_user_alive( id ) ) 
	{
		give_item( id, "weapon_usp" );
		cs_set_user_bpammo( id, CSW_USP, 100 );
	}
}
*/
public FwdGameDesc( ) {
	static const GameName[ ] = "Deathrun v3.0";
	
	forward_return( FMV_STRING, GameName );
	
	return FMRES_SUPERCEDE;
}

public FwdClientKill( const id ) {
	if( !g_bEnabled || !is_user_alive(id) )
		return FMRES_IGNORED;
	
	if( get_pcvar_num( g_pBlockKill ) || cs_get_user_team( id ) == CS_TEAM_T ) 
	{
		client_print( id, print_center, "%L", id, "DR_BLOCK_KILL" );
		client_print( id, print_console, "%L", id, "DR_BLOCK_KILL" );
		
		return FMRES_SUPERCEDE;
	}
	
	return FMRES_IGNORED;
}

public FwdImpulse_201( const id ) {
	if( g_bEnabled && get_pcvar_num( g_pBlockSpray ) ) {
		if( is_user_alive( id ) )
			client_print( id, print_center, "%L", id, "DR_BLOCK_SPRAY" );
		
		return PLUGIN_HANDLED_MAIN;
	}
	
	return PLUGIN_CONTINUE;
}

public FwdHamPlayerDamage( id, idInflictor, idAttacker, Float:flDamage, iDamageBits ) {
	if( get_pcvar_num( g_pNoFallDmg ) )
		if( iDamageBits & DMG_FALL )
			if( get_user_team( id ) == 1 )
				return HAM_SUPERCEDE;
	
	return HAM_IGNORED;
}

public MsgStatusIcon( msg_id, msg_dest, id ) {
	new szIcon[ 8 ];
	get_msg_arg_string( 2, szIcon, 7 );
	
	static const BuyZone[ ] = "buyzone";
	
	if( equal( szIcon, BuyZone ) ) {
		set_pdata_int( id, OFFSET_BZ, get_pdata_int( id, OFFSET_BZ, 5 ) & ~( 1 << 0 ), 5 );
		
		return PLUGIN_HANDLED;
	}
	
	return PLUGIN_CONTINUE;
}

public CmdRadio( id ) {
	if( get_pcvar_num( g_pBlockRadio ) )
		return PLUGIN_HANDLED_MAIN;
	
	return PLUGIN_CONTINUE;
}

public RestartRound( )
	set_pcvar_num( g_pSvRestart, 1 );

public clcmd_changeteam( id )
{
	if( is_logged( id ) )
	{
		menu_principal( id );
	}
	else
	{
		show_login_menu( id );
	}
	
	return PLUGIN_HANDLED;
}

public EventCurWeapon( id )
{
    if( g_iFreeRound != fNone )
    	engclient_cmd( id, "weapon_knife" );
}

public client_PreThink(id)
{
	if( !is_user_alive( id ) || !is_user_connected( id ) ) 
		return;
	
	switch(g_iFreeRound)
	{
		case fGrav: set_user_gravity(id, 0.5);
		case fDroga:
		{
			message_begin( MSG_ALL, gmsg_SetFOV, { 0, 0, 0 }, id );
			write_byte( 180 );
			message_end( ) ;
		}

	}
}
public ham_Use_Button( button, id, activator, iUseType, Float:flValue )
{
	if( !is_user_connected( id ) && !is_user_alive( id )  || !button || id > 32)
		return HAM_IGNORED;

	static szTarget[32];
	entity_get_string(button, EV_SZ_target, szTarget, charsmax(szTarget));

	if( get_user_team( id ) == 1 && !equal( szTarget, "end_map" ) )
	{
		if( g_iFreeRound != fNone )
		{
			if( iUseType == 2 && flValue == 1.0 && get_pdata_int( button, m_toggle_state, 4 ) == 1 ) 
			{
				set_hudmessage( 225, 225, 0, -1.0, 0.25, 0, 2.0, 2.0, 0.2, 0.2, 3 );
				show_hudmessage( id, "Es FreeRun^nNo puedes usar botones!" );
				
				return HAM_SUPERCEDE;
			}
		}
		else
		{
			if( !g_bButton[ button ] )
				g_bButton[ button ] = true;
			else 
			{
				client_print_color( id, print_team_blue, "Ya has usado esta TRAMPA" );
				emit_sound( button, CHAN_VOICE, "buttons/button11.wav", VOL_NORM, ATTN_NORM, 0, PITCH_NORM );
				
				return HAM_SUPERCEDE;
			}
		}
	}
	else if( get_user_team( id ) == 2 )
	{	 
		if( equal( szTarget, "end_map" ) &&  !g_iTouch[id] )
		{    
			g_iTouch[ id ] = 1;
	        
			set_dhudmessage( 255, 255, 0, -1.0, 0.65, 0, 1.0, 3.0 );
			show_dhudmessage( id, "TERMINASTE EL MAPA WACHIN!!!");

			static exp; exp = 20;

			fnCheckLevel( id, exp, 10 );
			checkLogro(id, 1);
			client_print_color(id, print_team_blue, "%s Ganaste 20 de Experiencia por llegar a la meta con %f segundos", g_szPrefix, g_fTiempo[id]);

			if(g_fRecord[id] > g_fTiempo[id])
			{
				new szQuery[ 128 ], iData[ 2 ];
				iData[ 0 ] = id;
				iData[ 1 ] = GUARDAR_DATOS;
				
				formatex( szQuery, charsmax( szQuery ), "UPDATE %s SET Record='%f' WHERE id_user='%d' AND MapName = ^"%s^"", g_szTableRecord, g_fTiempo[id], g_id[ id ], szMapName );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );

				client_print_color(0, print_team_blue, "%s El player %n rompió su record de %.2f segundos con %.2f segundos.", g_szPrefix, id, g_fRecord[ id ], g_fTiempo[ id ]);
				g_fRecord[ id ] = g_fTiempo[ id ];
			}
		}
	}
	return HAM_IGNORED;
}
public FW_Player_Jump(id)
{	
	if( !is_user_connected(id) || !is_user_alive(id) ) 
		return;
	
	if( g_iFreeRound != fBunny ) 
		return;
	
	entity_set_float(id, EV_FL_fuser2, 0.0);
	new flags = entity_get_int(id, EV_INT_flags);
	
	if (flags & FL_WATERJUMP)
		return;
	if ( entity_get_int(id, EV_INT_waterlevel) >= 2 )
		return;
	if ( !(flags & FL_ONGROUND) )
		return;
	
	new Float:velocity[3];
	entity_get_vector(id, EV_VEC_velocity, velocity);
	velocity[2] += 250.0;
	entity_set_vector(id, EV_VEC_velocity, velocity);
	
	entity_set_int(id, EV_INT_gaitsequence, 6);
}

public fnShowHud( id )
{
	id -= 7894;
	
	if( !is_user_connected( id ) )
		return;

	g_fTiempo[ id ]++;
	new szRango[ 60 ];
	
	if( g_iRango[ id ] >= charsmax( g_szRangos ) ) formatex( szRango, charsmax( szRango ), "%s", g_szRangos[ charsmax( g_szRangos ) ][ rango_name ] );
	else formatex( szRango, charsmax( szRango ), "%s", g_szRangos[ g_iRango[ id ] ][ rango_name ] );
	
	set_hudmessage( g_ColorHud[g_iHud[id]][hudColor][0], g_ColorHud[g_iHud[id]][hudColor][1], g_ColorHud[g_iHud[id]][hudColor][2], -1.0, 0.85, 0, 6.0, 1.0 );
	ShowSyncHudMsg( id, g_iHudPrincipal, "Nivel: %d - Experiencia: %d^nRango: %s - Monedas: %d^nTu Tiempo: %.2f", g_iLevel[ id ], g_iExp[ id ], szRango, g_iPuntos[ id ], g_fTiempo[id] );
	
	set_dhudmessage( 12, 201, 133, -1.0, 0.0, 1, 0.0, 1.0 );
	show_dhudmessage( 0, "[ DeathRun World ]^nBowser: %d | Mario: %d", gTScore, gCScore );
}
public cmdRecord(id)
{
	client_print_color(id, print_team_blue, "%s Tu record en este mapa es de %.2f", g_szPrefix, g_fRecord[id]);
}
public cmdTime(id)
{
    g_PlayedTime[id] += get_gametime() - g_pGameTime[id];
    g_pGameTime[id] = get_gametime();
    
    new days, hours, mins, segs;
    segs = floatround(g_PlayedTime[id]);
    
    mins = segs/60;
    hours = mins/60;
    days = hours/24;
    segs = segs-mins*60;
    mins = mins-hours*60;
    hours = hours-days*24;
    
    client_print_color( id, print_team_red, "Haz Jugado: %i Dia%s con %s%i:%s%i:%s%i", days, days == 1? "":"s", hours > 9? "":"0", hours, mins > 9? "":"0", mins, segs > 9? "":"0", segs);
} 
public fnFreeRound(id)
{
	if(!is_user_alive(id))
	{
		client_print_color( id, print_team_red, "Debes estar vivo para efecutar este comando" );
		return PLUGIN_HANDLED;
	}
	if( cs_get_user_team( id ) != CS_TEAM_T )
	{
		client_print_color( id, print_team_red, "Comando disponible solamente para los terroristas!" );
		return PLUGIN_HANDLED;
	}
	if( g_iFreeRound != fNone )
	{
		client_print_color( id, print_team_red, "Ya esta activada la ronda libre" );
		return PLUGIN_HANDLED;
	}
	if( g_fTimeFreeActived < get_gametime() )
	{
		client_print_color( id, print_team_red, "Se Acabo el tiempo para dar Ronda Libre" );
		return PLUGIN_HANDLED;
	}

	menu_freeRound(id);
	return PLUGIN_HANDLED;
}
public menu_principal(id)
{
	static menu, info[100];
	menu = menu_create("\wM E N U  \rD E A T H R U N^n\dby Hypnotize", "handler_principal");

	formatex(info, charsmax(info), "Tienda^n");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "Revivir");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "Estadisticas / Top15");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "Menu Logros");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "Estetica");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "Configuraciones^n");
	menu_additem(menu, info);

	formatex(info, charsmax(info), "\%sMenu Administrador", is_user_admin(id) ? "w" : "d");
	menu_additem(menu, info);

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_principal(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0: clcmdSHOP( id );
		case 1: menu_Lifes(id);
		case 2: menu_estadistica(id);
		case 3: menu_logros(id);
		case 4: menu_estetica(id);
		case 5: menu_config(id);
		case 6: menu_admin(id);
	}
	return PLUGIN_HANDLED;
}
public menu_config(id)
{
	new menu = menu_create("Menu Configuraciones", "handler_config");

	menu_additem(menu, "Colores de Hud");

	if(g_iTeamInvisible[ id ])
		menu_additem(menu, "Equipo Invisible: \rSI");
	else menu_additem(menu, "Equipo Invisible: \rNO");

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_config(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0: menuHud(id);
		case 1:
		{
			g_iTeamInvisible[ id ] = !g_iTeamInvisible[ id ];
			menu_config(id);
		}
	}
	return PLUGIN_HANDLED;
}
public menu_estetica(id)
{
	static menu, info[100], szItemName[50];
	menu = menu_create("ESTETICA", "handler_estetica");

	if( g_iTotalKnifes > 0 && g_iItem[id][KNIFE] >= 0 )
	{
		ArrayGetString( g_szItemName, g_iItem[id][KNIFE], szItemName, charsmax( szItemName ) );
		formatex(info, charsmax(info), "Knife \d| \r[\y%s\r]", szItemName);
	}else formatex(info, charsmax(info), "Knife \d| \r[\yNormal\r]");

	menu_additem(menu, info);

	if( g_iTotalUsps > 0 && g_iItem[id][USP] >= 0 )
	{
		ArrayGetString( g_szItemName, g_iItem[id][USP], szItemName, charsmax( szItemName ) );
		formatex(info, charsmax(info), "USP .45 Tactical \d| \r[\y%s\r]", szItemName);
	} else formatex(info, charsmax(info), "USP .45 Tactical \d| \r[\yNormal\r]");
	
	menu_additem(menu, info);

	if( g_iAccesorio[ id ] < 0 ) formatex(info, charsmax(info), "Sombrero \d| \r[\yNinguno\r]^n");
	else formatex(info, charsmax(info), "Sombrero \d| \r[\y%s\r]^n", ACCESORIOS[ g_iAccesorio[ id ] ][ hat_name ]);
	
	menu_additem(menu, info);

	formatex(info, charsmax(info), "\d- Los Cambios se verán reflejados a la siguiente ronda.");
	menu_addtext(menu, info);

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_estetica(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0: menu_api(id, KNIFE);
		case 1: menu_api(id, USP);
		case 2: menu_hats(id);
	}
	return PLUGIN_HANDLED;
}
public menu_hats(id)
{
	static menu, info[150]; menu = menu_create("Menu \yHats", "handler_hats");

	formatex(info, charsmax(info), "%sRemover%s accesorio", g_iAccesorio[ id ] < 0 ? "\d" : "\w", g_iAccesorio[ id ] < 0 ? "\d" : "\r" );
	menu_additem(menu, info);

	for ( new i = 0; i < sizeof( ACCESORIOS ); i++ )
	{
		if(g_iLevel[id] >= ACCESORIOS[i][hat_nivel])
		{
			if(g_iPuntos[id] >= ACCESORIOS[i][hat_precio])
			{
				if(g_iAccesorio[ id ] == i) formatex(info, charsmax(info), "\w%s \r[ACTUAL]", ACCESORIOS[i][hat_name]);
				else formatex(info, charsmax(info), "\w%s", ACCESORIOS[i][hat_name]);
			}
			else formatex(info, charsmax(info), "\d%s \r[\wPuntos\r: \y%d\r]", ACCESORIOS[i][hat_name], ACCESORIOS[i][hat_precio]);
		} else formatex(info, charsmax(info), "\d%s \r[\wNivel\r: \y%d\r]", ACCESORIOS[i][hat_name], ACCESORIOS[i][hat_nivel]);

		menu_additem(menu, info);
	}
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_hats(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	item -= 1;

	if( item < 0 )
	{
		if ( g_iAccesorio[ id ] < 0 ) 
		{
			client_print_color( id, print_team_red, "No llevas un accesorio.");
		}
		else
		{
			new ent = find_ent_by_owner( -1, ent_cm, id );
			entity_set_model( ent, "" );
			entity_set_edict( ent, EV_ENT_aiment, 0 );
			entity_set_int( ent, EV_INT_movetype, MOVETYPE_NONE );
			entity_set_origin( ent, Float:{8192.0,8192.0,8192.0} );
			
			g_iAccesorio[ id ] = item;
			client_print_color( id, print_team_red, "Ahora no llevas un accesorio.");

		}
		return PLUGIN_HANDLED;
	}
	
	if( g_iLevel[ id ] < ACCESORIOS[ item ][ hat_nivel ] || g_iPuntos[ id ] < ACCESORIOS[ item ][ hat_precio ] )
	{
		client_print_color( id, print_team_red, "No tienes lo suficiente.");
		return PLUGIN_HANDLED;
	}
	g_iAccesorio[ id ] = item;
	setHat(id, item);
	
	return PLUGIN_HANDLED;
}
public setHat(id, item)
{
	if(!is_user_connected(id))
		return;

	new szQuery[ 128 ], iData[ 3 ];
				
	iData[ 0 ] = id;
	iData[ 1 ] = CARGAR_HAT;
	iData[ 2 ] = item;

	formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE HatName=^"%s^" AND id_cuenta = %d", g_szTableHats, ACCESORIOS[ item ][ hat_name ], g_id[ id ] );
	SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
}
public menu_api(id, type)
{
	static menu, info[80], pts, lvl, adm, tipo, szItemName[50], szItem[3]; 
	menu = menu_create("Menu Items", "handler_api");

	for( new i = 0; i < g_iTotalItems; ++i )
	{
		formatex(szItem, charsmax(szItem), "%d", i);
		ArrayGetString( g_szItemName, i, szItemName, charsmax( szItemName ) );

		tipo = ArrayGetCell(g_iItemTipo, i);
		adm = ArrayGetCell(g_iItemAdmin, i);
		lvl = ArrayGetCell(g_iItemNivel, i);
		pts = ArrayGetCell(g_iItemPuntos, i);

		if(type != tipo)
			continue;

		if(adm == ADMIN_ALL)
		{
			if(g_iLevel[id] >= lvl)
			{
				if(g_iPuntos[id] >= pts) formatex(info, charsmax(info), "%s", szItemName);
				else formatex(info, charsmax(info), "\d%s - \r[ Puntos: %d ]", szItemName, pts);
			}
			else formatex(info, charsmax(info), "\d%s - \r[ Nivel: %d ]", szItemName, lvl);
		}
		else
		{
			if(get_user_flags(id) & adm)
			{
				if(g_iLevel[id] >= lvl)
				{
					if(g_iPuntos[id] >= pts) formatex(info, charsmax(info), "%s", szItemName);
					else formatex(info, charsmax(info), "\d%s - \r[ Puntos: %d ]", szItemName, pts);
				}
				else formatex(info, charsmax(info), "\d%s - \r[ Nivel: %d ]", szItemName, lvl);
			}
		}

		menu_additem(menu, info, szItem);
	}

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_api(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	new szData[ 20 ], Item[ 400 ], tipo, adm, lvl, pts, szItemName[32];
	static item_access, item_callback, item2;
	menu_item_getinfo( menu, item, item_access, szData,charsmax( szData ), Item, charsmax(Item), item_callback );
	
	item2 = str_to_num( szData );
	tipo = ArrayGetCell(g_iItemTipo, item2);
	adm = ArrayGetCell(g_iItemAdmin, item2);
	lvl = ArrayGetCell(g_iItemNivel, item2);
	pts = ArrayGetCell(g_iItemPuntos, item2);
	ArrayGetString(g_szItemName, item2, szItemName, charsmax(szItemName));

	if( g_iLevel[id] < lvl || g_iPuntos[id] < pts )
	{
		client_print_color(id, print_team_blue, "No tienes lo suficiente.");
		return PLUGIN_HANDLED;
	}
	
	new szQuery[ 128 ], iData[ 4 ];
				
	iData[ 0 ] = id;
	iData[ 1 ] = CARGAR_ITEMS;
	iData[ 2 ] = item2;
	iData[ 3 ] = tipo;

	formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE ItemName=^"%s^" AND ItemType =%d AND id_cuenta = %d", g_szTableItems, szItemName, tipo, g_id[ id ] );
	
	if( adm == ADMIN_ALL )
	{
		SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
		return PLUGIN_HANDLED;
	}
	if( get_user_flags(id) & adm )
	{
		SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
	}
	else
	{
		client_print_color(id, print_team_blue, "Necesitas ser administrador para acceder a este tipo.");
	}

	return PLUGIN_HANDLED;
}
public menu_admin(id)
{
	new menu = menu_create("Menu Admin", "handler_admin");

	if(get_user_flags(id) & ADMIN_RCON) menu_additem(menu, "Crear Coordenada");
	else menu_additem(menu, "\dCrear Coordenada");

	menu_additem(menu, "AMXMODMENU");

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_admin(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0:
		{
			if(get_user_flags(id) & ADMIN_RCON)
			{
				new iOrigin[3], Float:fOrigin[3], szMapa[35]; 

				get_user_origin(id, iOrigin, 3);
				get_mapname(szMapa, 34);

				IVecFVec(iOrigin, fOrigin); 
				CreateEnt(fOrigin);
				SaveEnt(fOrigin);
			}
			else client_print_color(id, print_team_blue, "No tienes acceso a esta opcion");
		}
		case 1:client_cmd(id, "amxmodmenu");
	}
	return PLUGIN_HANDLED;
}
public menu_freeRound(id)
{
	static menu; menu = menu_create("Menu \yFree Round", "handler_fr");
	menu_additem(menu, "Ronda Normal");
	menu_additem(menu, "Ronda BunnyHop");
	menu_additem(menu, "Ronda Droga");
	menu_additem(menu, "Ronda Grav");
	menu_additem(menu, "Ronda Respawn");

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_fr(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	item += 1;

	g_iFreeRound = item;
	client_print_color( 0, print_team_red, "El Player ^x04%n ^x01lanzo el free Round ^x04%s", 
		id, item == fNormal ? "Normal" : item == fBunny ? "BunnyHoop" : item == fDroga ? "Droga" : item == fGrav ? "Grav" : "Respawn" );
	
	remove_task(TASK_CONTEO);
	set_task(1.0, "endFreeRound", TASK_CONTEO, _, _, "b") ;
	return PLUGIN_HANDLED;
}
public endFreeRound()
{
	if(--g_iCount <= 0)
	{
		remove_task(TASK_CONTEO);

		for(new i = 1; i<= g_iMaxplayers; ++i)
		{
			if(!is_user_alive(i) || !is_user_connected(i))
				continue;

			switch(g_iFreeRound)
			{
				case fGrav: set_user_gravity(i, 0.8);
				//set_entvar(i, var_gravity, 0.8);
				
				case fDroga:
				{
					message_begin( MSG_ALL, gmsg_SetFOV, { 0, 0, 0 }, i );
					write_byte( 0 );
					message_end( ) ;
				}
			}
		}
		
		g_iFreeRound = fNone;
		client_print_color( 0, print_team_red, "Se acabo la ronda libre!");
		return;
	}
	set_dhudmessage(180, 180, 180, -1.0, 0.08, 0, 0.1, 0.4, 0.1, 1.0);
	show_dhudmessage(0, "FreeRound^n[ %d ]", g_iCount);
}
public menuHud(id)
{
	new menu = menu_create("Hud Colores", "handler_color");

	for(new i = 0; i < sizeof(g_ColorHud); ++i )
		menu_additem(menu, g_ColorHud[i][hudName]);
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_color(id, menu, item)
{
	if ( item == MENU_EXIT )
	{
	    menu_destroy(menu);
	    return PLUGIN_HANDLED;
	}
	g_iHud[id] = item;
	menuHud(id);
	client_print_color(id, print_team_default, "^x04%s^x01 Haz elegido el color ^x04%s", g_szPrefix, g_ColorHud[item][hudName]);
	return PLUGIN_HANDLED;
}

public clcmdSHOP( index )
{
	if(get_user_team(index) != 2)
		return PLUGIN_HANDLED;

	static menu, i, string[ 128 ], money;
	money = g_iPuntos[index];

	formatex( string, charsmax( string ), "\y[ Death Run Shop ]^n\wTienes \wPuntos: \r%d\w", money );
	menu = menu_create( string , "menu_shop" );
	
	for( i = 0 ; i < sizeof Shop ; ++i )
	{
		formatex( string, charsmax( string ), "\%s%s \w- \r[ \wPuntos\r: \y%d \r]", money >= Shop[i][PRECIO] ? "w":"d", Shop[i][NOMBRE], Shop[i][PRECIO]);
		
		menu_additem( menu , string, "" );
	}
	
	menu_display( index, menu );
	return PLUGIN_HANDLED;
}

public menu_shop( index, menu, item )
{
	if(item == MENU_EXIT || !is_user_connected(index) || get_user_team(index) != 2)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;

	}
	
	if( g_iPuntos[ index ] < Shop[ item ][ PRECIO ] )
	{
		client_print_color(index, print_team_red, "No te alcanza" );
		return PLUGIN_HANDLED;
	}
	
	switch( item )
	{
		case 0:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			give_item( index, "weapon_hegrenade" );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 1:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_footsteps( index, true );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 2:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_health(index, get_user_health( index ) + 100);
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 3:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			cs_set_user_armor( index, 200, CS_ARMOR_KEVLAR );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 4:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			kHasSpeed[ index ] = true;
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 5:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_gravity( index, 0.8 );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 6:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			cs_set_user_model( index, "artic" );
			set_task( 20.0, "RemoveMdl", index + TASK_REMOVEMDL );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 7:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
							
			set_user_rendering(index, kRenderFxGlowShell, random_num( 0, 255 ), random_num( 0, 255 ), random_num( 0, 255 ) );
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 8:
		{
			if( is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar Muerto para comprar esto" );
				return PLUGIN_HANDLED;
			}
							
			ExecuteHamB( Ham_CS_RoundRespawn, index );			
			
			client_print_color(index, print_team_red, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 9:
		{
			if( !is_user_alive( index ) )
			{
				client_print_color(index, print_team_red, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			give_item( index, "weapon_smokegrenade" );
		}
	}
	g_iPuntos[ index ] -= Shop[ item ][ PRECIO ];
	menu_destroy( menu );
	return PLUGIN_HANDLED;
}
public menu_Lifes(id)
{
	static menu, info[80];

	menu = menu_create("VIDAS", "handler_lifes");

	menu_additem(menu, "Si reviveme.");
	
	formatex(info, charsmax(info), "^n\w- Tienes \y%d \wVida%s.", 
		g_Lifes[id], g_Lifes[id] > 1 ? "s" : "");
	menu_addtext(menu, info);

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_lifes(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	if(item == 0)
	{
		fnRevivePlayer(id);
	}
	return PLUGIN_HANDLED;
}
public menu_estadistica(id)
{
	static menu, text[320];

	menu = menu_create("Estadistica", "handler_estadistica");

	g_PlayedTime[id] += get_gametime() - g_pGameTime[id];
	g_pGameTime[id] = get_gametime();

	new days, hours, mins, segs;
	segs = floatround(g_PlayedTime[id]);

	mins = segs/60;
	hours = mins/60;
	days = hours/24;
	segs = segs-mins*60;
	mins = mins-hours*60;
	hours = hours-days*24;

	menu_additem(menu, "Top15");
	formatex(text, charsmax(text), "^n\wNivel\r: \y%d - \wExperiencia\r: \y%d^n\wTiempo Jugado\r: \y%i Dia%s con %s%i:%s%i:%s%i^n\wMapas Terminados\r: \y%d^n\wAsesinados\r: \y%d^n\wPuntos\r: \y%d", 
		g_iLevel[id], g_iExp[id], days, days == 1? "":"s", hours > 9? "":"0", hours, mins > 9? "":"0", mins, segs > 9? "":"0", segs, g_iLlegadas[ id ], g_iKills[id], g_iPuntos[id]);
	menu_addtext(menu, text);
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_estadistica(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0: show_motd(id, "http://localhost/deathrun_world/top15.php");
	}
	return PLUGIN_HANDLED;
}
public menu_logros(id)
{
	static menu;

	menu = menu_create("Logros", "handler_logros");

	menu_additem(menu, "Por Kills");
	menu_additem(menu, "Por Mapas Acabados");
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_logros(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch(item)
	{
		case 0: menu_logrosKills(id);
		case 1: menu_logrosLlegados(id);
	}
	return PLUGIN_HANDLED;
}
public menu_logrosKills(id)
{
	static menu, text[120];

	menu = menu_create("Logros Kills", "handler_logrosLlegados");

	for(new i = 0; i < sizeof(MATADOS) ;++i )
	{
		if(g_iKills[ id ] >= MATADOS[i][logro_requerido])
			formatex(text, charsmax(text), "\w%s \r[DESBLOQUEADO]", MATADOS[i][nombre_logro]);
		else formatex(text, charsmax(text), "\d%s \r[BLOQUEADO]", MATADOS[i][nombre_logro]);

		menu_additem(menu, text);
	}
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public menu_logrosLlegados(id)
{
	static menu, text[120];

	menu = menu_create("Logros Mapas Terminados", "handler_logrosLlegados");

	for(new i = 0; i < sizeof(LLEGADAS) ;++i )
	{
		if(g_iLlegadas[ id ] >= LLEGADAS[i][logro_requerido])
			formatex(text, charsmax(text), "\w%s \r[DESBLOQUEADO]", LLEGADAS[i][nombre_logro]);
		else formatex(text, charsmax(text), "\d%s \r[BLOQUEADO]", LLEGADAS[i][nombre_logro]);

		menu_additem(menu, text);
	}
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}
public handler_logrosLlegados(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	return PLUGIN_HANDLED;
}
public fnCheckLevel( id, exp, pts )
{
	if( g_iLevel[ id ] >= LEVEL_MAX ) 
		return;
	
	static level;
	level = g_iLevel[ id ];

	g_iPuntos[ id ] += ( pts * g_iMultiplicador[ id ] );
	
	g_iExp[ id ] += ( exp * g_iMultiplicador[ id ] );
	
	while( g_iExp[ id ] >= next_level[ g_iLevel[ id ] >= charsmax(next_level) ? charsmax(next_level) : g_iLevel[ id ]-1 ] < LEVEL_MAX ) 
	{
		g_iLevel[ id ]++;
		g_iExp[ id ] = 0;
	}
	
	while( g_iLevel[ id ] >= g_szRangos[ g_iRango[ id ] >= charsmax(g_szRangos) ? charsmax(g_szRangos) : g_iRango[ id ] ][ rango_level ] && g_iRango[ id ] < sizeof(g_szRangos) ) 
	{
		g_iRango[ id ]++;
	}
	
	if ( level < g_iLevel[ id ] )
	{
		client_print_color( id, print_team_blue, "¡Felicitaciones! Subiste al nivel %d.", g_iLevel[ id ] );
	}
}
public checkLogro(id, type)
{
	switch(type)
	{
		case 0:
		{
			g_iKills[id]++;
			while(g_iKills[id] >= MATADOS[g_ieKills[id]][logro_requerido] && g_ieKills[id] < sizeof(MATADOS))
			{
				g_iPuntos[id] += MATADOS[g_ieKills[id]][logro_ganancia];
				client_print_color(id, print_team_default, "El player %n Hizo el logro %s", id, MATADOS[g_ieKills[id]][nombre_logro]);
				g_ieKills[id]++;
			}
		}
		case 1:
		{
			g_iLlegadas[id]++;
			while(g_iLlegadas[id] >= LLEGADAS[g_ieLlegadas[id]][logro_requerido] && g_ieLlegadas[id] < sizeof(LLEGADAS))
			{
				g_iPuntos[id] += LLEGADAS[g_ieLlegadas[id]][logro_ganancia];
				client_print_color(id, print_team_default, "El player %n Hizo el logro %s", id, LLEGADAS[g_ieLlegadas[id]][nombre_logro]);
				g_ieLlegadas[id]++;
			}
		}
	}
}
public set_Msg( id, msj[], cancion[] )
{
	playsound( id, cancion );
	
	client_print_color( id, print_team_red, "La ronda ^4%d^1 fue ^4%s.", gCountRound, msj );
}

public DataHandler( failstate, Handle:Query, error[ ], error2, data[ ], datasize, Float:flTime ) 
{
	switch( failstate ) 
	{
		case TQUERY_CONNECT_FAILED: 
		{
			log_to_file( "SQL_LOG_TQ.txt", "Error en la conexion al MySQL [%i]: %s", error2, error );
			return;
		}
		case TQUERY_QUERY_FAILED:
		log_to_file( "SQL_LOG_TQ.txt", "Error en la consulta al MySQL [%i]: %s", error2, error );
	}
	
	new id = data[ 0 ];
	
	if( !is_user_connected( id ) )
		return;
	
	switch( data[ 1 ] ) 
	{
		case LOGUEAR_USUARIO: 
		{
			if( SQL_NumResults( Query ) )
			{
				g_iLevel[ id ] = SQL_ReadResult( Query, 1 );
				g_iRango[ id ] = SQL_ReadResult( Query, 2 );
				g_iHud[ id ] = SQL_ReadResult( Query, 4 );
				g_iPuntos[ id ] = SQL_ReadResult( Query, 5 );
				g_iExp[ id ] = SQL_ReadResult( Query, 6 );
				g_iKills[ id ] = SQL_ReadResult( Query, 7 );
				g_ieKills[ id ] = SQL_ReadResult( Query, 8 );
				g_iLlegadas[ id ] = SQL_ReadResult( Query, 9 );
				g_ieLlegadas[ id ] = SQL_ReadResult( Query, 10 );
				SQL_ReadResult(Query, 11, _:g_PlayedTime[id]);

				set_task( 1.0, "fnShowHud", id+7894, .flags = "b" );

				new szQuery[ 128 ], iData[ 2 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = CARGAR_RECORD;
				
				formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE id_user = %d AND MapName = ^"%s^"", g_szTableRecord, g_id[ id ], szMapName );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );

				g_iStatus[ id ] = LOGUEADO;
				
			}
			else
			{
				new szQuery[ 128 ], iData[ 2 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = REGISTRAR_USUARIO;
				
				formatex( szQuery, charsmax( szQuery ), "INSERT INTO %s (id_cuenta, Level, Rango, Monedas, Exp) VALUES (%d, %d, %d, %d, %d)", szTable, g_id[ id ], g_iLevel[ id ], g_iRango[ id ], g_iPuntos[ id ], g_iExp[ id ] );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
			}
		}
		case REGISTRAR_USUARIO: 
		{
			if( failstate < TQUERY_SUCCESS ) 
			{
				console_print( id, "Error al crear un usuario: %s.", error );
			}
			else
			{
				guardar_datos( id );

				new szQuery[ 128 ], iData[ 2 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = LOGUEAR_USUARIO;

				formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE id_cuenta='%d'", szTable, g_id[ id ] );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
			}
		}
		case GUARDAR_DATOS:
		{
			if( failstate < TQUERY_SUCCESS )
				console_print( id, "Error en el guardado de datos." );
			else
			console_print( id, "Datos guardados." );
		}
		case CARGAR_ITEMS:
		{
			static tipo, item2, szItemName[30]; 
			item2 = data[ 2 ];
			tipo = data[ 3 ];

			if( SQL_NumResults(Query) )
			{
				SQL_ReadResult(Query, 2, szItemName, charsmax(szItemName));
				tipo = SQL_ReadResult(Query, 3);

				g_iItem[ id ][ tipo ] = item2;
				//give_api(id, g_iItem[ id ][ tipo ], tipo);
				client_print_color(id, print_team_blue, "El cambio se verá al revivir");
			}
			else
			{
				new szQuery[ 128 ], iData[ 4 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = COMPRAR_ITEMS;
				iData[ 2 ] = item2;
				iData[ 3 ] = tipo;

				ArrayGetString(g_szItemName, item2, szItemName, charsmax(szItemName));
				
				formatex( szQuery, charsmax( szQuery ), "INSERT INTO %s (ItemName, ItemType, id_cuenta) VALUES (^"%s^", %d, %d)", g_szTableItems, szItemName, tipo, g_id[ id ] );
				//client_print_color(0, print_team_blue, "%s", szQuery);
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
			}
		}
		case COMPRAR_ITEMS:
		{
			if( failstate < TQUERY_SUCCESS ) 
			{
				console_print( id, "Error al crear una compra: %s.", error );
			}
			else
			{
				static tipo, item2, pts; 

				tipo = data[ 3 ];
				item2 = data[ 2 ];

				g_iItem[ id ][ tipo ] = item2;

				pts = ArrayGetCell(g_iItemPuntos, item2);
				g_iPuntos[ id ] -= pts;
				//give_api(id, g_iItem[ id ][ tipo ], tipo);
				client_print_color(id, print_team_blue, "El cambio se verá al revivir");
			}
			
		}
		case CARGAR_RECORD:
		{
			if(SQL_NumResults(Query))
			{
				SQL_ReadResult(Query, 3, _:g_fRecord[id]);
			}
			else
			{
				new szQuery[ 128 ], iData[ 2 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = INSERTAR_RECORD;
				
				formatex( szQuery, charsmax( szQuery ), "INSERT INTO %s (id_user, MapName, Record) VALUES (%d, ^"%s^", %f)", g_szTableRecord, g_id[ id ], szMapName, g_fRecord[id] );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
			}
		}
		case INSERTAR_RECORD:
		{
			if( failstate < TQUERY_SUCCESS ) 
			{
				console_print( id, "Error al crear un RECORD: %s.", error );
			}
		}
		case COMPRAR_HAT:
		{
			static item; item = data[2];

			if( failstate < TQUERY_SUCCESS ) 
			{
				console_print( id, "Error al crear una compra: %s.", error );
			}
			else
			{
				new ent = find_ent_by_owner(-1, ent_cm, id);
				entity_set_model(ent, ACCESORIOS[item][hat_model]);
				entity_set_edict(ent, EV_ENT_aiment, id);
				entity_set_int(ent, EV_INT_movetype, MOVETYPE_FOLLOW);

				client_print_color(id, print_team_default, "%s Elegiste el hat %s", g_szPrefix, ACCESORIOS[item][hat_name]);
			}
		}
		case CARGAR_HAT:
		{
			static item; item = data[2];

			if( SQL_NumResults(Query) )
			{
				new ent = find_ent_by_owner(-1, ent_cm, id);
				entity_set_model(ent, ACCESORIOS[item][hat_model]);
				entity_set_edict(ent, EV_ENT_aiment, id);
				entity_set_int(ent, EV_INT_movetype, MOVETYPE_FOLLOW);

				client_print_color(id, print_team_default, "%s Elegiste el hat %s", g_szPrefix, ACCESORIOS[item][hat_name]);
			}
			else
			{
				new szQuery[ 128 ], iData[ 3 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = COMPRAR_HAT;
				//client_print(id, print_chat, "%d", item);
				iData[ 2 ] = item;

				
				formatex( szQuery, charsmax( szQuery ), "INSERT INTO %s (id_cuenta, HatName) VALUES (%d, ^"%s^")", g_szTableHats, g_id[ id ], ACCESORIOS[item][hat_name] );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, sizeof(iData) );
				
			}
		}
	}
	
}
public give_api(id, item, tipo)
{
	if(!is_user_alive(id))
		return; 

	static ret, szItemName[30];

	ExecuteForward(fw_Item_Selected, ret, id, item, tipo);
	if ( ret == PLUGIN_HANDLED )
		client_print_color(id, print_team_blue, "No puedes comprarlo ahora.");
	else
	{
		ArrayGetString(g_szItemName, item, szItemName, charsmax(szItemName));
		client_print_color(id, print_team_blue, "Has Elegido: %s", szItemName);
	}
}
public guardar_datos( id ) 
{
	if(!is_logged(id) || g_iStatus[ id ] != LOGUEADO)
		return;

	g_PlayedTime[id] += get_gametime() - g_pGameTime[id];

	new szQuery[ 400 ], iData[ 2 ];
	iData[ 0 ] = id;
	iData[ 1 ] = GUARDAR_DATOS;
	
	formatex( szQuery, charsmax( szQuery ), "UPDATE %s SET Kills='%d', LogroKills='%d', Llegadas='%d', LogroLlegadas='%d', TiempoJugado='%.2f', RangoName = ^"%s^", Hud='%d', Level='%d', Rango='%d', Monedas='%d', Exp='%d' WHERE id_cuenta='%d'", 
		szTable, g_iKills[ id ], g_ieKills[ id ], g_iLlegadas[ id ], g_ieLlegadas[ id ], g_PlayedTime[ id ], g_szRangos[ g_iRango[ id ] ][ rango_name ], g_iHud[ id ], g_iLevel[ id ], g_iRango[ id ], g_iPuntos[ id ], g_iExp[ id ], g_id[ id ] );
	SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
}

public native_item(plugin, params)
{
	new szNombre[32], type = get_param(4);
	get_string(1, szNombre, charsmax(szNombre));

	ArrayPushString(g_szItemName, szNombre);
	ArrayPushCell(g_iItemNivel, get_param(2));
	ArrayPushCell(g_iItemPuntos, get_param(3));
	ArrayPushCell(g_iItemTipo, type);
	ArrayPushCell(g_iItemAdmin, get_param(5));

	if(type == KNIFE)
		g_iTotalKnifes++;
	if(type == USP)
		g_iTotalUsps++;

	g_iTotalItems++;

	return g_iTotalItems-1; 
}
SaveEnt(const Float:Origin[3])
{
    static szCoordenada[90]; 
    formatex(szCoordenada, charsmax(szCoordenada),  "%.2f %.2f %.2f", Origin[0], Origin[1], Origin[2]);
    
    if(!file_exists(szRuta)) 
    {
        log_amx("[DR] Archivo '%s' No existe, pero lo creamos.", szRuta);
        write_file(szRuta, "; Archivo creado automaticamente");
        write_file(szRuta, "; Plugin Made By Hypnotize");
        write_file(szRuta, "; Las Coordenadas son:");
    }
    write_file(szRuta, szCoordenada);
}
public ReadEntity()
{
    if(!file_exists(szRuta)) {
        client_print(0, print_chat, "[ZE] Archivo '%s' NO Existe.", szRuta);
        return;
    }
    new iDat[40], iDat2[40], iDat3[40];
    new szLine[700], Float:iPoss[3];
    
    new file; file = fopen(szRuta, "r");
    while(!feof(file))
    {
        fgets(file, szLine, charsmax(szLine));
        
        if(szLine[0] == ';' || szLine[0] == '/' && szLine[1] == '/' || !szLine[0])
            continue;
        
        parse( szLine, iDat, charsmax(iDat), iDat2, charsmax(iDat2), iDat3, charsmax(iDat3));
        
        iPoss[ 0 ] = str_to_float( iDat );
        iPoss[ 1 ] = str_to_float( iDat2 );
        iPoss[ 2 ] = str_to_float( iDat3 );
        
        CreateEnt( iPoss );
    }
    fclose(file);
} 
public clcmd_say(id)
{
	static said[191];
	read_args(said, charsmax(said));
	remove_quotes(said);
	replace_all(said, charsmax(said), "%", " ");
	replace_all(said, charsmax(said), "#", " ");
	if (!ValidMessage(said, 1)) return PLUGIN_CONTINUE;
	static iRango; iRango = g_iRango[id] >= charsmax(g_szRangos) ? charsmax(g_szRangos) : g_iRango[id];
	
	static color[11], prefix[91];
	get_user_team(id, color, charsmax(color));
	
	formatex(prefix, charsmax(prefix), "%s ^x04%s^x01[ ^x04%s^x01 - ^x04%d^x01 ]^x03 %s", is_user_alive(id) ? "^x01" : "^x01*MUERTO* ", g_szTag[id], g_szRangos[iRango][rango_name], g_iLevel[id], g_szPlayerName[id]);
	
	if (is_user_admin(id)) format(said, charsmax(said), "^x04%s", said);
	format(said, charsmax(said), "%s^x01 :  %s", prefix, said);
	
	static i, team[11] ;
	for (i = 1; i <= g_iMaxplayers; i++) 
	{
		if (!is_user_connected(i)) 
			continue;

		get_user_team(i, team, charsmax(team));
		changeTeamInfo(i, color);
		writeMessage(i, said);
		changeTeamInfo(i, team);
		
	}
	return PLUGIN_HANDLED_MAIN;
}
public changeTeamInfo(player, team[])
{
	message_begin(MSG_ONE, g_msgTeamInfo, _, player);
	write_byte(player);
	write_string(team);
	message_end();
}

public writeMessage(player, message[])
{
	message_begin(MSG_ONE, g_msgSayText, {0, 0, 0}, player);
	write_byte(player);
	write_string(message);
	message_end();
}
stock ValidMessage(text[], maxcount) 
{
	static len, i, count;
	len = strlen(text);
	count = 0;
	
	if (!len)
		return false;
    
	for (i = 0; i < len; i++) 
	{
		if (text[i] != ' ') 
		{
			count++;
			if (count >= maxcount)
				return true;
		}
	}
	return false;
} 
CreateEnt(const Float:Origin[3])
{
	new ent = create_entity("func_button");
	        
	if(!is_valid_ent(ent))
	    return PLUGIN_CONTINUE;

	entity_set_string(ent, EV_SZ_classname, g_iEnt);
	entity_set_string(ent, EV_SZ_target, "end_map");
	entity_set_int(ent, EV_INT_solid, SOLID_BBOX);
	entity_set_model(ent, g_szModelButton);

	#define INFENT_MINS { -16.0, -16.0, 0.0 }
	#define INFENT_MAXS { 16.0, 16.0, 72.0 }
	entity_set_size( ent, Float:INFENT_MINS, Float:INFENT_MAXS );
	engfunc(EngFunc_SetOrigin, ent, Origin); 

	set_rendering(ent, kRenderFxGlowShell, 125, 125, 125, kRenderNormal, 16);

	drop_to_floor(ent);
	return PLUGIN_HANDLED;
}
fnGetAliveTeam(CsTeams:team)
{
	static iCTcount; iCTcount = 0;
	for( new i = 1; i <= g_iMaxplayers; i++ ) 
	{
		if( is_user_alive( i ) && cs_get_user_team( i ) == team )
			iCTcount++;
	}
	return iCTcount;
}

stock playsound( id, const sound[ ] )  
{  
	if( equal( sound[ strlen( sound ) -4 ], ".mp3" ) ) client_cmd( id, "mp3 play ^"%s^"", sound );
	else client_cmd( id, "spk ^"%s^"", sound );
} 
stock fnPrecacheSound( const sound[ ] )  
{  
	if( equal( sound[ strlen(sound) -4 ], ".mp3" ) ) precache_generic( sound );
	else precache_sound( sound );
}
stock prechache_player_model( const model[] )
{
	new info[ 200 ];
	formatex(info, charsmax(info), "models/player/%s/%s.mdl", model, model);
	precache_generic( info );
}
/*
rg_set_user_rendering(index, fx = kRenderFxNone, r = 255, g = 255, b = 255, render = kRenderNormal, Float:amount = 0.0)
{
	new Float:RenderColor[3];
	RenderColor[0] = float(r);
	RenderColor[1] = float(g);
	RenderColor[2] = float(b);

	set_entvar(index, var_renderfx, fx);
	set_entvar(index, var_rendercolor, RenderColor);
	set_entvar(index, var_rendermode, render);
	set_entvar(index, var_renderamt, amount);
}*/