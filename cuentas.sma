#include <amxmodx>
#include <sqlx>
#include <fakemeta>
/*

	CREATE TABLE zp_cuentas 
	(
		id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
		Pj varchar(34) NOT NULL UNIQUE KEY, 
		Password varchar(34) NOT NULL
	);

*/
enum
{
	REGISTRAR_CUENTA,
	LOGUEAR_CUENTA,
	CARGAR_DATOS,
	IS_REGISTER,
	TOTAL_CUENTAS
};

enum
{
	NO_REGISTRADO = 0,
	DESCONECTADO,
	REGISTRADO,
	LOGUEADO,
	MAX_STATUS
}

//No cambiar autor por m�s que lo uses para otro modo, no seas rata, no importa si lo reescribes media ves te bases en este
new const PluginName[] = "System Account";
new const PluginVersion[] = "1.0";
new const PluginAuthor[] = "Hypnotize";
//No cambiar autor por m�s que lo uses para otro modo, no seas rata, no importa si lo reescribes media ves te bases en este

//apartado para escribir el nombre del creador del mod
//area modificable
new const ModName[] = "";//nombre del mod
new const ModAuthor[] = "Hypnotize - divstarproject.com"; //ac� pones tu nombre si lo usaste para un modo tuyo
new const ModVersion[] = "1.0b";//versi�n del modo
new const g_szForo[] = "divstarproject.com";
//apartado para escribir el nombre del creador del mod
//area modificable

new const g_szTabla[ ] = "zp_cuentas";
new const g_szPrefijo[ ] = "[ZE]";

new const MYSQL_HOST[] = "";
new const MYSQL_USER[] = "";
new const MYSQL_PASS[] = "";
new const MYSQL_DATEBASE[] = "";

new Handle:g_hTuple;

new g_fwLogin;
new g_iTotalRegister;
new g_ShowMenu, g_VGUIMenu;
new g_estado[ 33 ];
new g_id[ 33 ];
new g_szPassword[ 33 ][ 34 ];
new g_szPlayerName[ 33 ][ 33 ];
const m_iVGUI = 510;

public plugin_init()  
{
	register_plugin( PluginName, PluginVersion, PluginAuthor );

	register_clcmd("CREAR_PASSWORD", "register_account");
	register_clcmd("LOGUEAR_PASSWORD", "login_account");

	register_forward(FM_ClientUserInfoChanged, "fw_ClientUserInfoChanged")
	
	g_VGUIMenu = get_user_msgid("VGUIMenu");
	g_ShowMenu = get_user_msgid("ShowMenu");

	register_message(g_ShowMenu, "message_showmenu");
	register_message(g_VGUIMenu, "message_VGUImenu");
	
	g_fwLogin = CreateMultiForward("user_login_post", ET_IGNORE, FP_CELL, FP_CELL);
	
	g_iTotalRegister = 0;

	Mysql_init( );
}

public plugin_natives()
{
	register_native("is_registered", "native_register", 1);
	register_native("is_logged", "native_logged", 1);
	register_native("show_login_menu", "native_login", 1)
}
public native_login(id)
	return show_login_menu(id);

public native_register(id)
	return g_estado[ id ] == REGISTRADO ? true : false;

public native_logged(id)
	return g_estado[ id ] == LOGUEADO ? true : false;

public message_VGUImenu( iMsgid, iDest, id ) 
{
	if( g_estado[ id ] >= LOGUEADO ||  get_msg_arg_int( 1 ) != 2 ) 
		return PLUGIN_CONTINUE;
	
	return PLUGIN_HANDLED;
}

public message_showmenu( iMsgid, iDest, id ) 
{
	if( g_estado[ id ] >= LOGUEADO )
		return PLUGIN_CONTINUE;
	
	static sMenuCode[ 33 ];
	get_msg_arg_string( 4, sMenuCode, charsmax( sMenuCode ) );
	
	if( containi( sMenuCode, "Team_Select" ) != -1 ) 
		return PLUGIN_HANDLED;
	
	return PLUGIN_CONTINUE;
}

public show_login_menu( id ) 
{
	static menu, info[200]; 

	formatex(info, charsmax(info), "\wBIENVENIDOS AL \y%s \r(%s)\w^n\wCreador: \r%s", ModName, ModVersion, ModAuthor);
	menu = menu_create(info, "menu_login" );
	
	if(g_estado[ id ] >= REGISTRADO)
		menu_additem( menu, "\dCrear una Cuenta" );
	else
		menu_additem( menu, "\wCrear una Cuenta" );

	if(g_estado[ id ] >= REGISTRADO)
		menu_additem( menu, "\wIdentificarse" );
	else
		menu_additem( menu, "\dIdentificarse" );
	
	formatex(info, charsmax(info), "^n\wCuentas Registradas\w: \y#%d", g_iTotalRegister);
	menu_addtext(menu, info);

	formatex(info, charsmax(info), "^n\wForo\r: \y%s", g_szForo);
	menu_addtext(menu, info);

	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}

public menu_login( id, menu, item ) 
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	switch( item ) 
	{
		case 0: 
		{
			if(g_estado[ id ] >= REGISTRADO)
				client_print(id, print_center, "%s Esta Cuenta ya esta registrada.", g_szPrefijo);
			else
				client_cmd( id, "messagemode CREAR_PASSWORD" );
		}
		case 1:
		{
			if(g_estado[ id ] >= REGISTRADO)
				client_cmd( id, "messagemode LOGUEAR_PASSWORD" );
			else
				client_print(id, print_center, "%s Tu cuenta aun no existe.", g_szPrefijo);
		}
	}
	return PLUGIN_HANDLED;
}

public register_account( id ) 
{
	read_args( g_szPassword[ id ], charsmax( g_szPassword[ ] ) );
	remove_quotes( g_szPassword[ id ] );
	trim( g_szPassword[ id ] );
	hash_string( g_szPassword[ id ], Hash_Md5, g_szPassword[ id ], charsmax( g_szPassword[] ) );
	
	new szQuery[ 256 ], iData[ 2 ];
	
	iData[ 0 ] = id;
	iData[ 1 ] = REGISTRAR_CUENTA;
	
	formatex( szQuery, charsmax( szQuery ), "INSERT INTO %s (Pj, Password) VALUES (^"%s^", ^"%s^")", g_szTabla, g_szPlayerName[ id ], g_szPassword[ id ] );
	SQL_ThreadQuery(g_hTuple, "DataHandler", szQuery, iData, 2);
	
	return PLUGIN_HANDLED;
}
public login_account( id ) 
{
	read_args( g_szPassword[ id ], charsmax( g_szPassword[ ] ) );
	remove_quotes( g_szPassword[ id ] );
	trim( g_szPassword[ id ] );
	hash_string( g_szPassword[ id ], Hash_Md5, g_szPassword[ id ], charsmax( g_szPassword[] ) );
	
	new szQuery[ 128 ], iData[ 2 ];
	
	iData[ 0 ] = id;
	iData[ 1 ] = LOGUEAR_CUENTA;
	
	formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE Pj=^"%s^" AND Password=^"%s^"", g_szTabla, g_szPlayerName[ id ], g_szPassword[ id ] );
	SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
	
	return PLUGIN_HANDLED;
}
public DataHandlerServer( failstate, Handle:Query, error[ ], error2, data[ ], datasize, Float:time ) 
{
	switch( failstate ) 
	{
		case TQUERY_CONNECT_FAILED: 
		{
			log_to_file( "SQL_LOG_TQ.txt", "Error en la conexion al MySQL [%i]: %s", error2, error );
			return;
		}
		case TQUERY_QUERY_FAILED:
			log_to_file( "SQL_LOG_TQ.txt", "Error en la consulta al MySQL [%i]: %s", error2, error );
	}
	switch( data[ 0 ] ) 
	{
		case TOTAL_CUENTAS:
		{
			if(SQL_NumResults( Query ))
			{
				g_iTotalRegister = SQL_ReadResult( Query, 0 );
			}
		}
	}
}
public DataHandler( failstate, Handle:Query, error[ ], error2, data[ ], datasize, Float:time ) 
{
	new id = data[ 0 ];
	
	if( !is_user_connected( id ) )
		return;

	switch( failstate ) 
	{
		case TQUERY_CONNECT_FAILED: 
		{
			log_to_file( "SQL_LOG_TQ.txt", "Error en la conexion al MySQL [%i]: %s", error2, error );
			return;
		}
		case TQUERY_QUERY_FAILED:
			log_to_file( "SQL_LOG_TQ.txt", "Error en la consulta al MySQL [%i]: %s", error2, error );
	}
	
	switch( data[ 1 ] ) 
	{
		case REGISTRAR_CUENTA: 
		{
			if( failstate < TQUERY_SUCCESS ) 
			{
				if( containi( error, "Pj" ) != -1 )
					client_print( id, print_chat, "%s El nombre de personaje esta en uso.", g_szPrefijo );
				else
					client_print( id, print_chat, "%s Error al crear la cuenta. Intente nuevamente.", g_szPrefijo );
				
				client_cmd( id, "spk buttons/button10.wav" );
				
				show_login_menu( id );
			}
			else 
			{
				client_print( id, print_chat, "%s Tu cuenta ha sido creada correctamente.", g_szPrefijo );
				
				new szQuery[ 128 ], iData[ 2 ];
				
				iData[ 0 ] = id;
				iData[ 1 ] = CARGAR_DATOS;

				g_estado[ id ] = REGISTRADO;
				
				formatex( szQuery, charsmax( szQuery ), "SELECT id FROM %s WHERE Pj=^"%s^"", g_szTabla, g_szPlayerName[ id ] );
				SQL_ThreadQuery( g_hTuple, "DataHandler", szQuery, iData, 2 );
			}
			
		}
		case LOGUEAR_CUENTA: 
		{
			if( SQL_NumResults( Query ) ) 
			{
				g_id[ id ] = SQL_ReadResult( Query, 0 );
				SQL_ReadResult( Query, 1, g_szPlayerName[ id ], charsmax( g_szPlayerName[ ] ) );					
				
				new iRet; ExecuteForward(g_fwLogin, iRet, id, g_id[ id ]);
			
				client_print( id, print_chat, "%s BIENVENIDO %s.", g_szPrefijo, g_szPlayerName[ id ]);
				client_print( id, print_chat, "%s TU ID DE CUENTA ES %d.", g_szPrefijo, g_id[ id ]);
				
				func_login_success( id );
			}
			else 
			{
				client_print( id, print_chat, "%s Usuario o Contraseñ incorrecta.", g_szPrefijo );
				client_cmd( id, "spk buttons/button10.wav" );
				
				show_login_menu( id );
			}
		}
		case CARGAR_DATOS: 
		{
			if( SQL_NumResults( Query ) ) 
			{
				g_id[ id ] = SQL_ReadResult( Query, 0 );
				
				new iRet; ExecuteForward(g_fwLogin, iRet, id, g_id[ id ]);

				client_print( id, print_chat, "%s BIENVENIDO %s.", g_szPrefijo, g_szPlayerName[ id ]);
				client_print( id, print_chat, "%s TU ID DE CUENTA ES %d", g_szPrefijo, g_id[ id ]);
				
				func_login_success( id );
			}
			else 
			{
				client_print( id, print_chat, "%s Error al cargar los datos, intente nuevamente.", g_szPrefijo );
				show_login_menu( id );
			}
		}
		case IS_REGISTER:
		{
			if( SQL_NumResults( Query ) )
			{
				g_estado[ id ] = REGISTRADO;
			}
			else
			{
				g_estado[ id ] = NO_REGISTRADO;
			}

			set_task(0.5, "show_login_menu", id);
		}
	}
}

public func_login_success( id ) 
{
	if( is_user_connected(id) )
	{
		static iRestore; iRestore = get_pdata_int( id, m_iVGUI );
			
		if( iRestore & (1<<0) )
			set_pdata_int( id, m_iVGUI, iRestore & ~(1<<0) );
		
		set_pdata_int( id, 365, 0 );
		set_msg_block( g_ShowMenu, BLOCK_SET );
		set_msg_block( g_VGUIMenu, BLOCK_SET );
		
		engclient_cmd( id, "jointeam", "5" );
		engclient_cmd( id, "joinclass", "5" );
		set_msg_block( g_ShowMenu, BLOCK_NOT );
		set_msg_block( g_VGUIMenu , BLOCK_NOT );
		
		if( iRestore & (1<<0) ) 
			set_pdata_int( id, m_iVGUI, iRestore );

		g_estado[ id ] = LOGUEADO;
		
		set_user_info( id, "name", g_szPlayerName[ id ] );
	}
		
}
public fw_ClientUserInfoChanged(id, buffer) 
{
    if (!is_user_connected(id)) 
    	return FMRES_IGNORED;
    
    static Name[32], Old[32];
    get_user_name(id, Name, charsmax(Name));

    if (equal(Old, Name)) 
    	return FMRES_IGNORED;
    
    set_user_info(id, "name", g_szPlayerName[ id ]);
    return FMRES_IGNORED;
} 

public client_putinserver( id )
{
	get_user_name( id, g_szPlayerName[ id ], charsmax( g_szPlayerName[ ] ) );

	check_register( id );
}

public check_register( id )
{
	new szQuery[ 256 ], iData[ 2 ];
	
	iData[ 0 ] = id;
	iData[ 1 ] = IS_REGISTER;
	
	formatex( szQuery, charsmax( szQuery ), "SELECT * FROM %s WHERE Pj = ^"%s^"", g_szTabla, g_szPlayerName[ id ]);
	SQL_ThreadQuery(g_hTuple, "DataHandler", szQuery, iData, 2);
}

public client_disconnected(  id ) 
{
	if( g_estado[ id ] ) 
	{
		g_estado[ id ] = DESCONECTADO;
	}
	g_szPassword[ id ][ 0 ] = EOS;
}

public Mysql_init()
{
	g_hTuple = SQL_MakeDbTuple( MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DATEBASE );
	
	if( !g_hTuple ) 
	{
		log_to_file( "SQL_ERROR.txt", "No se pudo conectar con la base de datos." );
		return pause( "a" );
	}
	new szQuery[ 256 ], iData[ 1 ];
	
	iData[ 0 ] = TOTAL_CUENTAS;
	
	formatex( szQuery, charsmax( szQuery ), "SELECT COUNT(*) FROM %s", g_szTabla);
	SQL_ThreadQuery(g_hTuple, "DataHandlerServer", szQuery, iData, 2);
	return PLUGIN_CONTINUE;
}

public plugin_end()
	SQL_FreeHandle( g_hTuple );
