#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <cstrike>
#include <deathrun_world>
#include <fun>

new g_item, g_iMaxplayers;
new g_iUsp[ 33 ];

new const szUsp_v[] = "v_knife";
new const szUsp_p[] = "p_knife";

public plugin_init()
{
	register_plugin("usp mdl", "1.0", "Hypnotize");
	// Add your own code here

	RegisterHam(Ham_Item_Deploy, "weapon_usp", "ham_UspDeployPost", true);
	register_logevent( "event_round_start", 2, "1=Round_Start" );

	g_item = register_item("Default", 1, 0, USP, ADMIN_ALL);

	g_iMaxplayers = get_maxplayers();
}
public event_round_start()
{
	for(new i = 1; i <= g_iMaxplayers; ++i)
	{
		g_iUsp[ i ] = 0;
	}
}
public plugin_precache()
{
  	static buffer[128];
	
	formatex( buffer, charsmax( buffer ), "models/%s.mdl", szUsp_v);
	precache_model(buffer);
	formatex( buffer, charsmax( buffer ), "models/%s.mdl", szUsp_p);
	precache_model(buffer);
}

public dr_item(id, item, tipo)
{
	if( tipo != USP )
		return;
		
	if( g_item != item )
		return;

	give_item(id, "weapon_usp");
	cs_set_user_bpammo(id, CSW_USP, 99);
	g_iUsp[ id ] = 1;
	return;
}	

public ham_UspDeployPost(wpn) 
{
    static id; id = get_weapon_ent_owner(wpn);
    
    if (!pev_valid(id) || !is_user_alive( id )) 
    	return;
    
    static WeaponID; WeaponID = cs_get_weapon_id(wpn); 
    
    static buffer[128];
    
    if(WeaponID == CSW_USP)
    {
        formatex( buffer, charsmax( buffer ), "models/%s.mdl", szUsp_v);
        set_pev(id, pev_viewmodel2, buffer );
        
        formatex( buffer, charsmax( buffer ), "models/%s.mdl", szUsp_p);
        set_pev(id, pev_weaponmodel2, buffer);
    }
}
stock get_weapon_ent_owner(ent)
{
    if (pev_valid(ent) != 2)
        return -1;
    
    return get_pdata_cbase(ent, 41, 4);
} 