#include <amxmodx>
#include <amxmisc>
#include <fun>
#include <cstrike>
#include <hamsandwich>
#include <fakemeta>
#include <deathrun_world>

new const P_INFO[][] = { "[DR] Shop, Knifes n Hats", "1.0", "kikizon" };

enum _:SHOPDATA { NOMBRE[ 50 ], PRECIO };

new const Shop[][SHOPDATA] =
{
	{ "He granada", 10 },
	{ "Botas silenciosas", 10 },
	{ "+100 HP", 10 },
	{ "+200 Armadura", 10 }, 
	{ "Speed \d( Hasta morir )", 10 },
	{ "Gravedad \d( Hasta morir )", 10 },
	{ "Camuflaje \d( \y20 \dsegundos )", 10 },
	{ "Glow \d( \rColor al Azar \d)", 10 },
	{ "Respawn", 10 }
};

new kHasSpeed[ 33 ];
new Ham:Ham_Player_ResetMaxSpeed = Ham_Item_PreFrame;

#define TASK_REMOVEMDL 1235
#define ID_RMVMDL ( taskid - TASK_REMOVEMDL )

public plugin_init() 
{
	register_plugin( P_INFO[0], P_INFO[1], P_INFO[2] );
	
	RegisterHam( Ham_Player_ResetMaxSpeed, "player", "ham_ResetMaxSpeedPost", true);
	RegisterHam( Ham_Spawn, "player", "ham_PlayerSpawn", true );
	
	register_clcmd( "say /shop", "clcmdSHOP" );
	register_clcmd( "say /tienda", "clcmdSHOP" );
	register_clcmd( "tienda_dr", "clcmdSHOP" );
}

public client_putinserver( index ) kHasSpeed[ index ] = false;

public client_disconnected( index ) remove_task( index + TASK_REMOVEMDL );

public clcmdSHOP( index )
{
	if(get_user_team(index) != 2)
		return PLUGIN_HANDLED;

	static menu, i, string[ 128 ], money;
	money = get_points( index );

	formatex( string, charsmax( string ), "\y[ Death Run Shop ]^n\wTienes \wPuntos: \r%d\w", get_points( index ) );
	menu = menu_create( string , "menu_shop" );
	
	for( i = 0 ; i < sizeof Shop ; ++i )
	{
		formatex( string, charsmax( string ), "\%s%s \w- \r[ \wPuntos\r: \y%d \r]", money >= Shop[i][PRECIO] ? "w":"d", Shop[i][NOMBRE], Shop[i][PRECIO]);
		
		menu_additem( menu , string, "" );
	}
	
	menu_display( index, menu );
	return PLUGIN_HANDLED;
}

public menu_shop( index, menu, item )
{
	if(item == MENU_EXIT || !is_user_connected(index) || get_user_team(index) != 2)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;

	}
	static money; money = get_points( index );
	
	if( money < Shop[ item ][ PRECIO ] )
	{
		ColorPrint( index, "No te alcanza" );
		return PLUGIN_HANDLED;
	}
	
	switch( item )
	{
		case 0:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			give_item( index, "weapon_hegrenade" );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 1:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_footsteps( index, 1 );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 2:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_health( index, get_user_health( index ) + 100 );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 3:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_armor( index, get_user_armor( index ) + 200 );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 4:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			kHasSpeed[ index ] = true;
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 5:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			set_user_gravity( index, 0.8 );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 6:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
			
			cs_set_user_model( index, "artic" );
			set_task( 20.0, "RemoveMdl", index + TASK_REMOVEMDL );
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 7:
		{
			if( !is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar vivo para comprar esto" );
				return PLUGIN_HANDLED;
			}
							
			set_user_rendering(index, kRenderFxGlowShell, random_num( 0, 255 ), random_num( 0, 255 ), random_num( 0, 255 ));
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
		case 8:
		{
			if( is_user_alive( index ) )
			{
				ColorPrint( index, "Necesitas estar Muerto para comprar esto" );
				return PLUGIN_HANDLED;
			}
							
			ExecuteHamB( Ham_CS_RoundRespawn, index );			
			
			set_points( index, money - Shop[item][PRECIO] );
			ColorPrint( index, "Compraste: !g%s", Shop[ item ][ NOMBRE ] );
		}
	}
	
	menu_destroy( menu );
	return PLUGIN_HANDLED;
}

public ham_ResetMaxSpeedPost(index) set_player_speed(index);

public ham_PlayerSpawn( index )
{
	if( !is_user_alive(index) ) return;
	
	set_user_rendering( index );
}

set_player_speed(index) 
{
	if(!is_user_alive( index ) ) return;
	
	if( kHasSpeed[ index ] )
		set_pev(index, pev_maxspeed, 290.0 );
	else
		set_pev(index, pev_maxspeed, 245.0 );
	
}

public RemoveMdl( taskid )
{
	cs_set_user_model( ID_RMVMDL, "sas" );
}

stock ColorPrint(const id, const input[], any:...)
{	
	new count = 1, players[32];
	static msg[191], len;
	
	len = formatex(msg, charsmax(msg), "!g[ DR SHOP ]!n ");
	vformat(msg[len], 190 - len, input, 3);
	
	replace_all(msg, 190, "!g", "^4");
	replace_all(msg, 190, "!n", "^1");
	replace_all(msg, 190, "!t", "^3");
	
	
	if(id) 
		players[0] = id;
	else 
		get_players(players, count, "ch");
         	
	for (new i = 0; i < count; ++i)
	{
		if(is_user_connected(players[i]))
		{
			message_begin(MSG_ONE_UNRELIABLE, get_user_msgid( "SayText" ) , _, players[i]);  
			write_byte(players[i]);
			write_string(msg);
			message_end();				
		}		
	}

}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang3082\\ f0\\ fs16 \n\\ par }
*/
